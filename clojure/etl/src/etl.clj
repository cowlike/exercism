(ns etl
  (:require [clojure.string :refer [lower-case]]))

(defn mk-map [k v]
  (println k v)
  (->> v
    (map lower-case)
    (map #(hash-map % k))))

(defn transform
    "argument looks like {1 [\"WORLD\" \"GSCHOOLERS\"]}"
    [old-map]
    (->> (map mk-map (keys old-map) (vals old-map))
      flatten
      (apply merge)))
