(ns pascals-triangle)

(defn grow [acc [x y & xs]]
  (cond
    (nil? x) acc
    (nil? y) (cons x acc)
    :else  (grow (cons (+' x y) acc) (cons y xs))))

(def triangle (iterate (partial grow [1]) [1]))

(defn row [n]
  (if (<= n 0)
    []
    (first (drop (dec n) triangle))))