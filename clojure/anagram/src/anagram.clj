(ns anagram
  (:require [clojure.string :refer [lower-case]]))

(defn anagrams-for [word candidates]
  (let [normalize (comp sort lower-case)
        n= (fn [w c] (not= (lower-case w) (lower-case c)))
        anagram (fn [w c] (and (n= w c) (= (normalize w) (normalize c))))]
    (filter (partial anagram word) candidates)))
