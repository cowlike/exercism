(ns play.core
  (:require [clojure.string :refer [split triml]])
  (:gen-class))

(defn cmp [[xt yt] [x y]]
  (cond
    (> x y) [(inc xt) yt]
    (< x y) [xt (inc yt)]
    :otherwise [xt yt]))

; (with-in-str "1 2 3\n4 5 6\n" (score))
(defn score []
  (let [nums (fn [] (map read-string (split (read-line) #"\s+")))
        as (nums)
        bs (nums)
        [ta tb] (reduce cmp [0 0] (map list as bs))]
    (println ta tb)))

; (with-in-str "3\n4 5 6\n" (nsum))
(defn nsum []
  (let [n (read-string (read-line))
        nums (map read-string (split (read-line) #"\s+"))]
    (reduce + 0 (take n nums))))

(defn minimax []
  (let [xs (vec (map read-string (split (read-line) #"\s+")))
        x (range 5)
        sublists (for [x (range 5)] (concat (take x xs), (drop (inc x) xs)))
        sums (map (partial reduce +) sublists)]
    (println (apply min sums) (apply max sums))))

(defn birthday-cake-candles []
  (let [n (read-string (read-line))
        heights (take n (map read-string (split (read-line) #"\s+")))]
    (println (count (filter (partial = (apply max heights)) heights)))))
; (with-in-str "4\n3 1 2 3\n" (birthday-cake-candles))

(defn mk-hour [h p]
  (let [hh (mod h 12)
        h24 (+ hh (if (= p "P") 12 0))]
    (format "%02d" h24)))

(defn time-format []
  (let [time (read-line)
        matcher (re-matcher #"(\d+):(\d+):(\d+)([AP])" time)
        [h m s ap] (rest (re-find matcher))]
    (println (format "%s:%s:%s" (mk-hour (read-string h) ap) m s))))
;;(with-in-str "03:54:10P" (time-format))

(defn prisoners []
  (let [n (read-string (read-line))]
    (doseq [_ (range n)]
      (let [[n m s] (map read-string (split (read-line) #"\s+"))]
        (println (inc (mod (+ (dec m) (dec s)) n)))))))
;;(with-in-str "2\n10 10 1\n5 2 2\n" (prisoners))

(defn circular-array-rotation []
  (let [read-arr #(map read-string (split (read-line) #"\s+"))
        [n k q] (read-arr)
        xs (take n (read-arr))]
    (doseq [_ (range q)]
      (let [i (read-string (read-line))]
        (println (nth xs (mod (- i k) n)))))))

(defn -main
  "playground for trying things"
  [& args]
  (println "Hello, World!"))
