(ns pangram
  (require [clojure.string :refer [lower-case]]))

(defn pangram? [input]
  (->> input
    lower-case
    (re-seq #"[a-z]")
    distinct
    count
    (= 26)))
