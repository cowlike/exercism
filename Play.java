/*
** See https://www.stephanboyer.com/post/115/higher-rank-and-higher-kinded-types
*/

interface Chooser {
    <T> T choose(T a, T b);
}

class Play {
    static String chooseStringWeirdChooser(Chooser c, String head, String tail) {
        if (c.choose(true, false)) {
            return c.choose(head, tail);
        } else {
            return c.choose(tail, head);
        }
    }

    // Can't use a lambda for this, they don't work for generics methods
    static class FirstChooser implements Chooser {
        public <T> T choose(T a, T b) {
            return a;
        }
    }

    public static void main(String[] args) {
        System.out.println("Choosen with chooser: " + chooseStringWeirdChooser(new FirstChooser(), "emacs", "vim"));
    }
}