func toLimit(_ n: Int, inMultiples xs: [Int]) -> Int {
    let isMult = { n in xs.contains(where: { n % $0 == 0 }) }
    return (1..<n).filter(isMult).reduce(0, +)
}