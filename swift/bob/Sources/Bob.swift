import Foundation

func trim(_ input:String) -> String {
    return input.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

func hasAlpha(_ input:String) -> Bool {
    return input.rangeOfCharacter(from: CharacterSet.letters) != nil
}

func hey(_ input: String) -> String {
    var ret = "Whatever."

    if trim(input).isEmpty { ret = "Fine. Be that way!" }
    else if hasAlpha(input) && input.uppercased() == input { ret = "Whoa, chill out!" }
    else if input.hasSuffix("?") { ret = "Sure." }

    return ret
}