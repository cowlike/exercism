func compute(_ input: String, against: String) -> Optional<Int> {
    let xs = input.characters
    let ys = against.characters
    var n:Int?

    if xs.count == ys.count { 
        n = zip(xs, ys).filter {x,y in x != y}.count 
    }
    return n
}
