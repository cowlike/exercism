enum GrainsError: Error {
    case inputTooLow (message: String)
    case inputTooHigh (message: String)
}

func square(_ n: Int) throws -> UInt64 {
    let errMsg = { (n:Int) in "Input[\(n)] invalid. Input should be between 1 and 64 (inclusive)" }
    if n < 1 { throw GrainsError.inputTooLow(message: errMsg(n)) }
    else if n > 64 { throw GrainsError.inputTooHigh(message: errMsg(n)) }

    return 1 << (UInt64(n) - 1)
}

var total:UInt64 { return try! (1...64).map(square).reduce(0, +) }