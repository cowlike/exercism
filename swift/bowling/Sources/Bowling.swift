
class Bowling{
   
    enum BowlingError: Error {
        case invalidNumberOfPins
        case tooManyPinsInFrame
        case gameInProgress
        case gameIsOver
    }
 
    private struct Frame {
        var rolls: [Int]
        var isStrike: Bool { return rolls.count >= 1 && rolls[0] == 10 }
        var isSpare: Bool { return rolls.count >= 2 && rolls.prefix(2).sum() == 10 }
        var notComplete: Bool { return !isStrike && rolls.count <= 1 }
        var additionalRolls: Int
        init(_ r: [Int], _ aR: Int) {
            rolls = r
            additionalRolls = aR
        }
        init(){ self.init([], 0) }
    }
   
    private var frames:[Frame] = [Frame()]
    private var frameCount: Int { return frames.count }
   
    init(){}
   
    func roll(pins: Int) throws {
        if pins > 10 || pins < 0 { throw BowlingError.invalidNumberOfPins }
       
        if frameCount == 10 {
            if frames[frames.count - 1].additionalRolls == 0 && !frames[frames.count - 1].notComplete { throw BowlingError.gameIsOver }
            if frames[frames.count - 1].isStrike && (frames[frames.count - 1].rolls.sum() - 10 + (pins % 10)) > 10 { throw BowlingError.tooManyPinsInFrame }
        }
        else if frames[frames.count - 1].notComplete && frames[frames.count - 1].rolls.sum() + pins > 10 { throw BowlingError.tooManyPinsInFrame }
       
        frames[frames.count - 1].rolls.append(pins)
       
        if frameCount != 10 {
            frames = frames.map( { return $0.additionalRolls == 0 ? $0 : Frame($0.rolls + [pins], $0.additionalRolls - 1) } )
        }
        else {
            let t = Array(frames.prefix(9)).map( { return $0.additionalRolls == 0 ? $0 : Frame($0.rolls + [pins], $0.additionalRolls - 1) } )
            frames = t + [frames[frames.count - 1]]
           
            if frames[frames.count - 1].additionalRolls != 0 {frames[frames.count - 1].additionalRolls -= 1}
        }
       
        if frameCount != 10 {
            if pins == 10 { frames[frames.count - 1].additionalRolls = 2 }
            else if frames[frames.count - 1].rolls.sum() == 10 { frames[frames.count - 1].additionalRolls = 1 }
            if !frames[frames.count - 1].notComplete { frames.append(Frame()) }
        }
        else {
            if frames[frames.count - 1].isStrike && frames[frames.count - 1].rolls.count == 1 { frames[frames.count - 1].additionalRolls = 2 }
            else if frames[frames.count - 1].isSpare && frames[frames.count - 1].rolls.count == 2 { frames[frames.count - 1].additionalRolls = 1 }
        }
    }
   
    func score() throws -> Int {
        print(frames.flatMap( {return $0.rolls} ))
        if frames.count != 10 || frames.last!.additionalRolls > 0 { throw BowlingError.gameInProgress }
        return frames.reduce(0) { return $0 + $1.rolls.sum() }
    }
}
 
extension Array where Iterator.Element == Int { func sum() -> Int { return self.reduce(0, +) } }
extension ArraySlice where Iterator.Element == Int { func sum() -> Int { return self.reduce(0, +) } }
