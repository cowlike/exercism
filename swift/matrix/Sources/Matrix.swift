import Foundation

extension Array {
    func tail() -> [Element] {
        return self.isEmpty ? [] : Array(self.dropFirst())
    }

    func cons(_ x: Element) -> [Element] {
        return [x] + self
    }
}

struct Matrix {
    let rows: [[Int]]
    var columns: [[Int]] = []

    init(_ input: String) {
        rows = input.components(separatedBy: "\n").map {
            $0.components(separatedBy: " ").flatMap { Int($0) }
        }
        columns = Matrix.transpose(rows)
    }

    static func transpose<T>(_ xs: [[T]]) -> [[T]] {
        if (xs.isEmpty || xs[0].isEmpty) {
            return []
        }
        else {
            let remaining = Matrix.transpose(xs.map { $0.tail() })
            return remaining.cons(xs.map { $0[0] })
        }
    }
}