struct Raindrops {
    let sounds: String
    
    init(_ n: Int) {
        let divisors = [(3,"Pling"), (5,"Plang"), (7,"Plong")]
        let str = divisors.reduce("") {
            acc, v in acc + (n % v.0 == 0 ? v.1 : "")
        }
        sounds = str.isEmpty ? String(n) : str
    }
}
