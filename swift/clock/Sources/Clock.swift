import Foundation

class Clock: CustomStringConvertible, Equatable {
    var hours = 0
    var minutes = 0

    var description: String {
        return String(format: "%0.2d:%0.2d", hours, minutes)
    }

    init(hours: Int, minutes: Int) {
        let (h,m) = Clock.normalize(hours, minutes)
        self.hours = h
        self.minutes = m
    }

    convenience init (hours: Int) {
        self.init(hours: hours, minutes: 0)
    }

    static func == (lhs: Clock, rhs: Clock) -> Bool {
        return lhs.hours == rhs.hours && lhs.minutes == rhs.minutes
    }

    static func normalize(_ h: Int, _ m: Int) -> (Int,Int) {
        var hh = (h + (m / 60)) % 24 
        var mm = m % 60
        if mm < 0 { 
            mm += 60; 
            hh -= 1;
        }
        if hh < 0 { 
            hh += 24 
        }
        return (hh, mm)
    }

    func add(minutes: Int) -> Clock {
        let (h,m) = Clock.normalize(hours, self.minutes + minutes)
        self.hours = h
        self.minutes = m
        return self
    }

    func subtract(minutes: Int) -> Clock {
        return add(minutes: -minutes)
    }
}