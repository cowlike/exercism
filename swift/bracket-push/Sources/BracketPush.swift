extension String {
    var head: Character? {
        return self.characters.first
    }
    var tail: String {
        return String(self.characters.dropFirst())
    }
}

func paired(text: String, _ acc: [Character] = []) -> Bool {
    func butLast<T>(_ xs: [T]) -> [T] {
        return xs.isEmpty ? [] : Array(xs.prefix(xs.count - 1))
    }
    let matches: [(Character,Character)] = [("{","}"), ("[","]"), ("(", ")")]

    if text.isEmpty {
        return acc.isEmpty
    }
    
    let c = text.head!
    guard let match = matches.first(where: {$0 == c || $1 == c}) else { 
        //no match
        return paired(text: text.tail, acc)
    }

    if (match.0 == c) {
        //matched left side
        return paired(text: text.tail, acc + [c])
    }

    //matched right side
    guard let lastChar = acc.last else { return false }
    return match.0 == lastChar ? paired(text: text.tail, butLast(acc)) : false
}
