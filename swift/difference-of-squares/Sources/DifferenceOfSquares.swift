struct Squares {
    let squareOfSums: Int
    let sumOfSquares: Int
    let differenceOfSquares: Int

    init (_ n:Int) {
        let sum = (1...n).reduce(0, (+))
        squareOfSums =  sum * sum
        sumOfSquares = (1...n).map { $0 * $0 }.reduce(0, (+))
        differenceOfSquares = squareOfSums - sumOfSquares
    }
}