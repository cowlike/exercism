struct GradeSchool {
    var roster = [Int:[String]]()

    var sortedRoster: [Int:[String]] { return roster }

    func studentsInGrade(_ g: Int) -> [String] {
        guard let grade = roster[g] else { return [] }
        return grade
    }

    mutating func addStudent(_ name: String, grade: Int) {
        var oldList = studentsInGrade(grade)
        oldList.append(name)
        roster[grade] = oldList.sorted()
    }
}