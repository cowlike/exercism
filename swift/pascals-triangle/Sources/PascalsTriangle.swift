struct PascalsTriangle {
    let rows: [[Int]]

    init(_ n: Int) { 
        rows = nTimes(times: n, [], PascalsTriangle.getRow) 
    }

    static func getRow(_ currentRow: [Int]) -> [Int] {
        let sum = { (xs:[Int]) in xs.reduce(0, +) }
        return currentRow.isEmpty ? [1] : 
            (currentRow.windowed(2).reduce([1], { $0 + [sum($1)] }) + [1])
    }
}

func nTimes<T> (times: Int, _ initial: T, _ f: ((_:T) -> T)) -> [T] {
    guard times > 0 else { return [] }
    
    return (1 ..< times).reduce([f(initial)]) { 
        (acc,_) in return acc + [f(acc.last!)] 
    }
}

extension Array {
    func windowed(_ size: Int, _ index: Int = 0) -> [[Element]] {
        if (size + index > self.count) { 
            return [] 
        }
        else {
            let x = self.startIndex + index
            return [Array(self [x ..< x + size])] + windowed(size, index + 1)
        }   
    }
}
