func flattenArray<T>(_ arr: [Any?]) -> [T] {	

	func add(_ acc: [T], _ v: Any) -> [T] {
		if let arr = v as? [Any] {
			return arr.isEmpty ? acc : add(add(acc, arr[0]), Array(arr.dropFirst()))
		}
		guard let vv = v as? T else { return acc }
		return acc + [vv]
	}	
	return arr.reduce([], add)
}
