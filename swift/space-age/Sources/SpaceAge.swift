import Foundation

struct SpaceAge {
    init(_ s: Double) {
        seconds = s
    }

    let seconds: Double

    var onMercury: Double { return toYears(0.2408467) }
    var onVenus: Double { return toYears(0.61519726) }
    var onEarth: Double { return toYears(1.0) }
    var onMars: Double { return toYears(1.8808158) }
    var onJupiter: Double { return toYears(11.862615) }
    var onSaturn: Double { return toYears(29.447498) }
    var onUranus: Double { return toYears(84.016846) }
    var onNeptune: Double { return toYears(164.79132) }

    func toYears(_ factor: Double) -> Double {
        let v = (seconds / 31557600) / factor
        return round(v * 100) / 100
    }
}
