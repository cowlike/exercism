import Foundation

struct Gigasecond {
    let description: String

    init?(from: String) {
        let  formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dt = formatter.date(from: from)
        if var d = dt { 
            d.addTimeInterval(1000000000.0)
            description = formatter.string(from: d)
        }
        else {
            return nil
        }        
    }
}
