struct ETL {
    static func transform(_ input: [Int:[String]]) -> [String:Int] {
        var result = [String:Int]()
        input.forEach { k,v in v.forEach { result[$0.lowercased()] = k } }
        return result
    }
}
