struct Scrabble {
    let score: Int

    static func score(_ word: String?) -> Int {
        let data: [Character:Int] = [
            "A":1,"E":1,"I":1,"O":1,"U":1,
            "L":1,"N":1,"R":1,"S":1,"T":1,
            "D":2,"G":2,"B":3,"C":3,"M":3,
            "P":3,"F":4,"H":4,"V":4,"W":4,
            "Y":4,"K":5,"J":8,"X":8,"Q":10,"Z":10]
        
        guard let w = word else { return 0 }
        return w.uppercased().characters.reduce(0) {
            guard let points = data[$1] else { return $0 }
            return $0 + points
        }
    }

    init(_ word: String?) {
        score = Scrabble.score(word)
    }
}

