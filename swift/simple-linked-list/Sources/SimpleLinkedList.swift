class Element<T>: Sequence {
    struct ListIterator<T> : IteratorProtocol {
        private var element: Element<T>?
        init(_ el: Element<T>) { element = el }
        
        mutating func next() -> Element<T>? {
            let e = element
            element = e?.next
            return e
        }
    }

    let value: T?
    let next: Element?

    init(_ v: T? = nil, _ n: Element<T>? = nil) {
        value = v
        next = n
    }
	
    func makeIterator() -> ListIterator<T> { return ListIterator(self) }

    func toArray() -> [T] {
        return self.reduce([]) {
            guard let v = $1.value else { return $0 }
            return $0 + [v]
        }
    }

    static func fromArray(_ arr: [T]) -> Element<T> {
        return arr.reversed().reduce(Element()) { Element($1, $0) }
    }
	
    func reverseElements() -> Element<T> {
        return self.reduce(Element()) { Element($1.value, $0) }
    }
}