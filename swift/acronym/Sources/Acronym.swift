import Foundation

func abbreviate(_ input: String) -> String {
    let str = input.unicodeScalars.reduce("") {
        let s = String($1)
        guard let accLast = $0.unicodeScalars.last else { return s }   

        let b = CharacterSet.uppercaseLetters.contains($1) &&
                CharacterSet.lowercaseLetters.contains(accLast)
        return $0 + (b ? " " + s : s)
    }
    
    return str.components(separatedBy: CharacterSet.letters.inverted).reduce("") { 
	    return $1.isEmpty ? $0 : $0 + String($1.unicodeScalars.first!).uppercased()
    }
}