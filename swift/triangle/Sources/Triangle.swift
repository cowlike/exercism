struct Triangle {
    let kind: String

    init(_ a: Double, _ b: Double, _ c: Double) {
        func tri(_ xs: [Double]) -> String {
            return 
                xs[0] + xs[1] <= xs[2] || xs[0] == 0 ? "ErrorKind"
                : xs[0] == xs[1] && xs[1] == xs[2] ? "Equilateral"
                : xs[0] == xs[1] || xs[1] == xs[2] ? "Isosceles"
                : "Scalene"
        }
        kind = tri([a,b,c].sorted())
    }
}