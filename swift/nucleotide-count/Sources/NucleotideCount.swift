extension Sequence {
    func all(_ f: (_:Self.Iterator.Element) -> Bool) -> Bool {
        for x in self {
            if !f(x) { return false }
        }
        return true
    }
}

struct DNA {
    var nucleotides = ["A":0, "T":0, "C":0, "G":0]

    init?(strand: String) {
        if !strand.isEmpty && 
            !(strand.unicodeScalars.all { nucleotides.keys.contains(String($0)) }) {
            return nil
        }
        
        strand.unicodeScalars.forEach { key in 
            if let v = nucleotides[String(key)] {
                nucleotides.updateValue(v + 1, forKey: String(key))
            }
        }
    }

    func count(_ s: String) -> Int {
        guard let c = nucleotides[s] else { return 0 }
        return c
    }
        
    func counts() -> [String:Int] {
        return nucleotides
    }
}
