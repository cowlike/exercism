enum ConversionError: Error {
    case invalid (message: String)
}

class Nucleotide {
    var rna = ""
    
    init(_ dna: String) throws {
	rna = try dna.map {c in 
            if let cs = toRNA(c) { return cs } else { 
		throw ConversionError.invalid(message: "bleh") 
	    }
	}.joined()
    }

    func complementOfDNA() -> String { return rna }

    func toRNA(_ c: Character) -> String? {
        var ret: String?

        switch c {
        case "G": ret = "C"
        case "C": ret = "G"
        case "T": ret = "A"
        case "A": ret = "U"
        default:  ret = nil
        }

        return ret
    }
}

