enum BinarySearchError: Error { case unsorted }

struct BinarySearch {
    let list: [Int]
    let middle: Int
    
    init(_ arr: [Int]) throws {
        if arr.sorted() != arr { throw BinarySearchError.unsorted }
        list = arr
        middle = arr.count / 2
    }

    private func binarySearch(_ input: [Int], _ value: Int, _ left: Int, _ right: Int) -> Int? {
        if left > right { return nil }

        let mid = (left + right) / 2
        if value == input[mid] { return mid }
        if value > input[mid] { return binarySearch(input, value, mid + 1, right) }
        else { return binarySearch(input, value, left, mid - 1) }
    }

    func searchFor(_ value: Int) -> Int? {
        return binarySearch(list, value, 0, list.count - 1)
    }
}
