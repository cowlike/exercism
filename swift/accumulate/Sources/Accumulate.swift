extension Array {
    func accumulate<T>(_ f: (_:Element) -> T) -> Array<T> {
        var result: [T] = []
        for x in self {
            result.append(f(x))
        }
        return result
    }
}
