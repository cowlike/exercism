import Foundation

class Robot {
    var name: String

	private static let letters = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ".characters)
    private static let digits = Array("01234567890".characters)
    private static var seed: UInt32 = 0
	
    private static func randGen() -> ((Int, [Character]) -> String) {
		return 
        { (x, xs) in 
            Robot.seed += 1
			srand(Robot.seed)
			return Array(1...x).reduce("", {acc, _ in acc + String(xs[Int(rand()) % xs.count])})
		}
	}

	private static func mkName() -> String { 
		return Robot.randGen()(2,letters) + Robot.randGen()(3,digits) 
	}
	
    func resetName() -> () { name = Robot.mkName() }
    
    init () {
        name = Robot.mkName()
    }
}
