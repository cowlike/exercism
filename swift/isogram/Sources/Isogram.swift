import Foundation

func isIsogram(_ input: String) -> Bool {
    let chars = input.uppercased().unicodeScalars.filter { CharacterSet.letters.contains($0) }
    return chars.count == Set(chars).count
}