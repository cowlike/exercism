struct Year {
    var calendarYear = 0

    var isLeapYear: Bool {
        return calendarYear % 400 == 0 || (calendarYear % 4 == 0 && calendarYear % 100 != 0) 
    }
}
