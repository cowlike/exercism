def distance(dna1, dna2):
    if len(dna1) != len(dna2):
        return 0
    else:
        return sum([a != b for (a,b) in zip(dna1,dna2)])