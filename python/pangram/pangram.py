import string

# def is_pangram(sentence):
#     chars = [c for c in set(sentence.lower()) if c in string.ascii_lowercase]
#     return len(chars) == 26

def is_pangram(sentence):
    return set(sentence.lower()) >= set(string.ascii_lowercase)
