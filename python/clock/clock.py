class Clock:
    def __init__ (self, hour, minute):
        self.hour, self.minute = Clock.hour_min(hour, minute)

    def __str__ (self):
        return "%2.2d:%2.2d" % (self.hour, self.minute)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def add(self, minutes):
        self.hour, self.minute = Clock.hour_min(self.hour, self.minute + minutes)
        return self

    def hour_min(hour, minute):
        return (hour + (minute // 60)) % 24, minute % 60