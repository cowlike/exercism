object SumOfMultiples {
  fun sum(factors: Set<Int>, bound: Int) =
      factors.flatMap { multiples(it, bound) }.toSet().sum()

  fun multiples(x: Int, bound: Int) = (x until bound).step(x).toList()
}
