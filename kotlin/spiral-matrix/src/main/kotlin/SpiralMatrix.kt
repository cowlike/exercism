object SpiralMatrix {
  fun ofSize(n: Int) = when {
    n == 0 -> emptyArray<Int>()
    else -> mkArray(n)
  }

  private fun mkArray(n: Int): Array<IntArray> {
    val matrix = Array(n) { IntArray(n) }
    val fill = fillMatrix(matrix)

    fill(3, n - 1)
    for (x in (n - 2).downTo(1)) {
      fill(2, x)
    }

    return matrix
  }

  private fun fillMatrix(matrix: Array<IntArray>): (Int,Int) -> Unit {
    var v = 1
    var pos = Pair(0, 0)
    var dir = Direction.RIGHT
    matrix[pos.first][pos.second] = v++

    return fun (x: Int, n: Int) {
      repeat(x) {
        repeat(n) {
          move(dir, pos).let {
            pos = it
            matrix[pos.first][pos.second] = v++
          }
        }
        dir = turn(dir)
      }
    }
  }

  private enum class Direction { RIGHT, LEFT, UP, DOWN }

  private fun turn(dir: Direction) = when (dir) {
    Direction.RIGHT -> Direction.DOWN
    Direction.LEFT -> Direction.UP
    Direction.UP -> Direction.RIGHT
    Direction.DOWN -> Direction.LEFT
  }

  private fun move(dir: Direction, p: Pair<Int, Int>) = p.let { (row, col) ->
    when (dir) {
      Direction.RIGHT -> Pair(row, col + 1)
      Direction.LEFT -> Pair(row, col - 1)
      Direction.DOWN -> Pair(row + 1, col)
      Direction.UP -> Pair(row - 1, col)
    }
  }
}