class MinesweeperBoard(val board: List<String>) {

  fun withNumbers() = board.mapIndexed { row, columns ->
    columns.mapIndexed { col, c ->
      if (c == ' ') sum(row, col) else c.toString()
    }.joinToString(separator = "")
  }

  fun sum(row: Int, col: Int): String {
    val neighbors = (-1..1).map { row - 1 to col + it } +
        (-1..1).map { row + 1 to col + it } +
        listOf(row to col - 1, row to col + 1)

    return neighbors.sumBy { (row, col) ->
      when {
        row == -1 || row >= board.size -> 0
        col == -1 || col >= board[row].length -> 0
        board[row][col] == '*' -> 1
        else -> 0
      }
    }.let { if (it == 0) " " else String.format("%d", it) }
  }
}
