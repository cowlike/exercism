fun <T> List<T>.customAppend(xs: List<T>) =
    xs.customFoldLeft(this) { acc,v -> acc + v }

fun <T> List<List<T>>.customConcat() =
    this.customFoldLeft(emptyList<T>()) { acc,v -> acc + v }

fun <T> List<T>.customFilter(pred: (T) -> Boolean) =
    this.customFoldLeft(emptyList<T>()) {
      acc,v -> if (pred(v)) acc + v else acc
    }

val <T> List<T>.customSize: Int
  get() = this.customFoldLeft(0) { acc,v -> acc + 1 }

fun <T, U> List<T>.customMap(f: (T) -> U) =
    this.customFoldLeft(emptyList<U>()) { acc,v -> acc + f(v) }

fun <T, U> List<T>.customFoldLeft(acc: U, f: (U, T) -> U): U =
    if (this.isEmpty()) acc
    else this.drop(1).customFoldLeft(f(acc, this.first()), f)

fun <T, U> List<T>.customFoldRight(acc: U, f: (T, U) -> U): U =
    if (this.isEmpty()) acc
    else this.dropLast(1).customFoldRight(f(this.last(), acc), f)

fun <T> List<T>.customReverse() =
    this.customFoldRight(emptyList<T>()) { v,acc -> acc + v }
