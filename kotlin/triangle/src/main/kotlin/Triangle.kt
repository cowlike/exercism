class Triangle(val x: Double, val y: Double, val z: Double) {

  constructor(x: Int, y: Int, z: Int) :
      this(x.toDouble(), y.toDouble(), z.toDouble())

  enum class Tri { ISOSCELES, SCALENE, EQUILATERAL }

  val triType = listOf(x,y,z).sorted().let { (a,b,c) ->
    when {
      a + b <= c || a == 0.0 -> throw IllegalArgumentException("DEGENERATE")
      a == b && b == c       -> Tri.EQUILATERAL
      a == b || b == c       -> Tri.ISOSCELES
      else                   -> Tri.SCALENE
    }
  }

  val isIsosceles = triType == Tri.ISOSCELES || triType == Tri.EQUILATERAL
  val isScalene = triType == Tri.SCALENE
  val isEquilateral = triType == Tri.EQUILATERAL
}
