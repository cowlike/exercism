import java.util.Random

class Robot {
  var name = nextName()

  fun reset() {
    name = nextName()
  }

  companion object {
    var allNames = (0 until 26 * 26 * 10 * 10 * 10).toSet()

    fun nextName(): String {
      require (allNames.isNotEmpty()) { "All names are used" }

      val x = allNames.elementAt(Random().nextInt(allNames.size))
      allNames -= x
      return mkName(x)
    }

    fun mkName(n: Int): String {
      val aHi = 'A' + (n / 26000 % 26)
      val aLo = 'A' + (n / 1000 % 26)
      return "$aHi$aLo${n / 100 % 10}${n / 10 % 10}${n % 10}"
    }
  }
}
