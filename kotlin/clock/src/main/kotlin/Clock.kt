class Clock(var hour: Int, var minute: Int) {
  init {
    normalize(hour, minute)
  }

  fun add(m: Int) = normalize(hour, minute + m)

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other?.javaClass != javaClass) return false
    other as Clock
    return hour == other.hour && minute == other.minute
  }

  override fun toString() = String.format("%02d:%02d", hour, minute)

  fun normalize(h: Int, m: Int) {
    hour = (h + (m / 60)) % 24
    minute = m % 60

    if (minute < 0) {
      hour -= 1
      minute += 60
    }

    if (hour < 0) hour += 24
  }
}
