object Atbash {
  val pairs = ('a'..'z').let { it.zip(it.reversed()) } +
      ('0'..'9').let { it.zip(it) }

  fun encode(s: String) =
      s.fold("") { acc, c ->
        acc + (pairs.find { c.toLowerCase() == it.first } ?.second ?: "")
      }.chunked(5).joinToString(" ")

  fun decode(s: String) = s.fold("") { acc, c ->
    if (c.isLetterOrDigit()) acc + pairs.find { c == it.second } ?.first ?: ""
    else acc
  }
}
