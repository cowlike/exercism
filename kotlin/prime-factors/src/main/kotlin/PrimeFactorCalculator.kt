object PrimeFactorCalculator {

  fun primeFactors(n: Int) = findPrimes(n.toLong()).map { it.toInt() }

  fun primeFactors(n: Long) = findPrimes(n)

  tailrec fun findPrimes(v: Long, divisor: Long = 2,
                         acc: List<Number> = emptyList()): List<Number> =
      when {
        (v % divisor == 0L) -> findPrimes(v / divisor, divisor, acc + divisor)
        divisor >= v -> acc
        else -> findPrimes(v, divisor + 1, acc)
      }
}