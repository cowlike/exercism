class Series(val str: String) {
  init {
    require(str.all { it.isDigit() })
  }

  fun getLargestProduct(win: Int): Int {
    require(win > -1)
    require(win <= str.length)

    fun product(s: String) = s.windowed(1).map { it.toInt() }.reduce { acc, it -> acc * it }

    return if (win == 0) 1 else str.windowed(win).map { product(it) }.max() ?: 0
  }
}
