class NumberSpeller {
  fun say(n: Long): String {
    require(n >= 0L) { "Input must be non-negative" }
    require(n < 1000000000000L) { "Input must be less than 1000000000000" }

    fun getScale(idx: Int, n: Long): String {
      val scaleWords = listOf("billion", "million", "thousand", "")
      return if (n > 0) "${hundred(n.toInt())} ${scaleWords[idx]} " else ""
    }

    return if (n == 0L) "zero" else {
      scaledValues(n)
          .mapIndexed(::getScale)
          .joinToString("")
          .trim()
    }
  }

  private fun scaledValues(n: Long, scale: Int = 1e9.toInt()): List<Long> =
    if (scale == 1) listOf(n)
    else listOf(n / scale) + scaledValues(n % scale, scale / 1000)

  private fun hundred(n: Int): String {
    val low = listOf(
        "", "one", "two", "three", "four", "five",
        "six", "seven", "eight", "nine", "ten",
        "eleven", "twelve", "thirteen", "fourteen", "fifteen",
        "sixteen", "seventeen", "eighteen", "nineteen")

    fun append(n: Int) = if (n % 10 > 0) "-" + low[n % 10] else ""

    return when {
      n in 0L..19L -> low[n]
      n < 30L -> "twenty" + append(n)
      n < 40L -> "thirty" + append(n)
      n < 50L -> "forty" + append(n)
      n < 60L -> "fifty" + append(n)
      n < 70L -> "sixty" + append(n)
      n < 80L -> "seventy" + append(n)
      n < 90L -> "eighty" + append(n)
      n < 100L -> "ninety" + append(n)
      else -> "${hundred(n.toInt() / 100)} hundred ${hundred(n.toInt() % 100)}"
    }
  }
}