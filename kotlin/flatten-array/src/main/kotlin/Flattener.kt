object Flattener {
  fun flatten(lst: List<*>): List<*> =
      lst.fold(emptyList<Any?>()) { acc, v ->
        when {
          v is List<*> -> acc + flatten(v)
          v == null -> acc
          else -> acc + v
        }
      }
}
