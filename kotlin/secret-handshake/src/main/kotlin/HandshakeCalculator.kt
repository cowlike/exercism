object HandshakeCalculator {
    fun calculateHandshake(n: Int) = enumValues<Signal>()
            .fold(emptyList()) { acc: List<Signal>, v ->
                if (n.bitSet(v.ordinal)) acc + v else acc
            }
            .let { if (n.bitSet(4)) it.reversed() else it }
}

infix fun Int.pow(exp: Int) = (1..exp).fold(1) { acc, _ -> acc * this }

fun Int.bitSet(bitPosition: Int) = this and (2 pow bitPosition) != 0
