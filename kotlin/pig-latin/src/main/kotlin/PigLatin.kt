object PigLatin {
  private val vowels = "aeiou"

  private val qu = "(qu|.qu)(.*)".toRegex()

  private val nextVowel = "(.[^y$vowels]*)(.*)".toRegex()

  private fun String.startsWithVowel() =
      this.matches("([$vowels]|y[^$vowels]).*".toRegex())

  infix fun <T> List<T>?.`??`(n: Int): T? = this?.get(n)

  private fun translateWord(w: String) = when {
    w.startsWithVowel() -> w + "ay"

    w.matches(qu) -> qu.matchEntire(w)?.groupValues.let {
      (it `??` 2) + (it `??` 1) + "ay"
    }

    else -> nextVowel.matchEntire(w)?.groupValues.let {
      val suffix = it `??` 2
      if (suffix == "ay") (it `??` 0) + "ay"
      else suffix + (it `??` 1) + "ay"
    }
  }

  fun translate(input: String) = input
      .toLowerCase()
      .split("""\s+""".toRegex())
      .map(::translateWord)
      .joinToString(" ")
}
