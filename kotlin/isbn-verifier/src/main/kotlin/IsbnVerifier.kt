class IsbnVerifier {

  private fun int(c: Char) = if (c == 'X') 10 else c.toInt() - '0'.toInt()
  private val isbnFormat = """^\d{9}[\dX]$""".toRegex()

  fun isValid(isbn: String) =
      isbn.replace("-", "")
          .let { isbnFormat.matchEntire(it)?.value }
          ?.run { (toList() zip (10 downTo 1)).sumBy { (x, y) -> int(x) * y } % 11 == 0 }
          ?: false
}
