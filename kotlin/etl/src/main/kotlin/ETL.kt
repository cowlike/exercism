object ETL {
  fun transform(old: Map<Int,List<Char>>) = old.flatMap {
    (k,v) -> v.map { it.toLowerCase() to k }
  }.toMap()
}