
enum class Classification {
    DEFICIENT, PERFECT, ABUNDANT
}

fun classify(n: Int) : Classification {
    require(n > 0) { "Argument must be > 0" }

    val aliquotSum = (1..n / 2)
            .sumBy { if (n % it == 0) it else 0 }

    return when aliquotSum.compareTo(n).let {
        when {
            it < 0  -> Classification.DEFICIENT
            it == 0 -> Classification.PERFECT
            else    -> Classification.ABUNDANT
        }
    }
}

