object BracketPush {
  private val pairs = listOf('('.to(')'), '['.to(']'), '{'.to('}')).toMap()

  private fun Char.matches(other: Char) = pairs[this] == other

  private tailrec fun count(braces: List<Char>, s: String): Boolean =
      if (s.isEmpty()) braces.isEmpty() else {
        val first = s.first()
        val rest = s.drop(1)

        when (first) {
          in pairs.keys -> count(braces + first, rest)
          in pairs.values ->
            if (braces.lastOrNull()?.matches(first) == true) {
              count(braces.dropLast(1), rest)
            } else false
          else -> count(braces, rest)
        }
      }

  fun isValid(input: String) = count(emptyList(), input)
}
