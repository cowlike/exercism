object BinarySearch {
  tailrec fun <T> search(input: List<Comparable<T>>,
                         searchTerm: Comparable<T>,
                         offset: Int = 0): Int =

      if (input.isEmpty() || input.size == 1 && input[0] != searchTerm) -1
      else {
        val mid = input.size / 2
        when (compareValues(searchTerm, input[mid])) {
          1 -> search(input.subList(mid, input.size), searchTerm, offset + mid)
          -1 -> search(input.subList(0, mid), searchTerm, 0)
          else -> offset + mid
        }
      }
}
