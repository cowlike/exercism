object ScrabbleScore {
  fun scoreWord(s: String) = s.toUpperCase().sumBy(::scoreLetter)
}

private fun scoreLetter(c: Char) =
    when (c) {
      in "AEIOULNRST" -> 1
      in "DG"         -> 2
      in "BCMP"       -> 3
      in "FHVWY"      -> 4
      'K'             -> 5
      in "JX"         -> 8
      in "QZ"         -> 10
      else            -> 0
    }
