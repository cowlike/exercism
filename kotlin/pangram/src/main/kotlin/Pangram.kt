class Pangram {
    companion object {
        val isPangram = { p:String -> (('a'..'z') - p.toLowerCase().toSet()).isEmpty() }
    }
}
