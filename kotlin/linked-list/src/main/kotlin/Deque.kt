class Deque<T> {
  data class Node<T> (var value: T, var prev: Node<T>?, var next: Node<T>?)

  fun <T> mkNode(t: T, prev: Node<T>? = null, next: Node<T>? = null) =
      Node(value = t, prev = prev, next = next)

  var first: Node<T>? = null

  var last: Node<T>? = null

  fun push(v: T) {
    val f = first
    first = mkNode(v, next = f)
    f?.prev = first
    if (last == null) last = first
  }

  fun pop(): T? {
    val result = first?.value
    first = first?.next
    return result
  }

  fun unshift(v: T) {
    val l = last
    last = mkNode(v, prev = l)
    l?.next = last
    if (first == null) first = last
  }

  fun shift(): T? {
    val result = last?.value
    last = last?.prev
    return result
  }
}