class Matrix(val values: List<List<Int>>) {

  val saddlePoints: Set<MatrixCoordinate>

  private fun maxInRow(v: Int, r: Int) = values[r].max()
      .let { it == null || v >= it }

  private fun minInCol(v: Int, c: Int) =
      (0 until values.size).map { values[it][c] }.min()
          .let { it == null || v <= it }

  init {
    val points = values.mapIndexed { r, rows ->
      rows.mapIndexed { c, v -> Triple(r, c, v) }
    }.flatten()

    saddlePoints = points.fold(emptySet()) { acc, (r, c, v) ->
      if (maxInRow(v, r) && minInCol(v, c)) acc + MatrixCoordinate(r, c)
      else acc
    }
  }
}
