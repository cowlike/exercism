fun reverse(s: String): String =
    s.fold("") { acc, c -> c + acc }

