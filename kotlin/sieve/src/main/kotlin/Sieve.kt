object Sieve {
  val primesUpTo = this::primesUpToV3

  // Using mutation to mark the non-primes
  fun primesUpToV3(limit: Int): List<Int> {
    val xs = 2..limit

    val potentialPrimes = xs
      .map { Pair(it, true) }
      .toMap()
      .toMutableMap()

    xs.forEach {
      (it + it..limit step it)
        .forEach { idx -> potentialPrimes[idx] = false }
    }

    return potentialPrimes
      .filter { it.value }
      .map { it.key }
  }

  // Start out by generating all the non-primes
  fun primesUpToV2(limit: Int): List<Int> {
    val nonPrimes =
      (2..limit / 2).flatMap {
        generateSequence(it) { x -> x + it }
          .drop(1)
          .takeWhile { y -> y <= limit }
          .toList()
      }
    return (2..limit) - nonPrimes
  }

  // Original from years ago that no longer works
  fun primesUpToV1(n: Int = 2): Sequence<Int> = sequence<Int> {
    yield(n)
    yieldAll(primesUpToV1(n + 1).filter { x: Int -> x % n != 0 })
  }
}
