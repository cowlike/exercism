class Squares(val n: Int) {
    val ns = 1..n

    fun sumOfSquares() = ns.sumBy {it * it}

    fun squareOfSum() = ns.sum().let {it * it}

    fun difference() = squareOfSum() - sumOfSquares()
}