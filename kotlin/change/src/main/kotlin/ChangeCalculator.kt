class ChangeCalculator(val coins: List<Int>) {

  fun computeMostEfficientChange(total: Int): List<Int> {
    require(total >= 0) { "Negative totals are not allowed." }

    return coins.sortedDescending()
        .tails()
        .fold(emptyList<List<Int>>()) { acc, v ->
          acc + computeChange(total, total, v, emptyList())
        }.filter {
          it.sum() == total
        }.let {
          require(it.isNotEmpty()) {
            "The total $total cannot be represented in the given currency."
          }
          it.minBy { it.size }!!
        }
  }
}

private fun computeChange(total: Int, remaining: Int, coinsLeft: List<Int>,
                          change: List<Int>): List<List<Int>> =
    when {
      remaining == 0 -> listOf(change.sorted())

      coinsLeft.isEmpty() -> emptyList()

      remaining < coinsLeft.first() ->
        computeChange(total, remaining, coinsLeft.drop(1), change)

      else ->
        computeChange(
            total, remaining - coinsLeft.first(),
            coinsLeft, change + coinsLeft.first()).let {
          if (it.isEmpty())
            computeChange(total, remaining, coinsLeft.drop(1), change)
          else it
        }
    }

fun <T> List<T>.tails(): Sequence<List<T>> =
    generateSequence(this) { if (it.size <= 1) null else it.drop(1) }
