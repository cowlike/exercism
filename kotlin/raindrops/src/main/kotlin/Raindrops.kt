class Raindrops {
    companion object {
        val divisors = listOf(Pair(3, "Pling"), Pair(5, "Plang"), Pair(7, "Plong"))

        fun divBy(n: Int, p: Pair<Int, String>) = if (n % p.first == 0) p.second else ""

        fun convert(n: Int): String {
            val result = divisors.map { divBy(n, it) }.joinToString("")
            return if (result.isEmpty()) n.toString() else result
        }
    }
}