object Luhn {
  fun isValid(cc: String) =
      cc.filterNot {it == ' '}.let {
        it.all(Char::isDigit)
            && it.length > 1
            && validChecksum(it)
      }

  fun validChecksum(cc: String) =
      cc.reversed().mapIndexed { i, c ->
        (c - '0').let { if (i % 2 != 0 && it != 9) (it * 2 % 9) else it }
      }.sum() % 10 == 0
}