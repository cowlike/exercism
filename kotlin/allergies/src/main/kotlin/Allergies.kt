class Allergies(val allergies: Int) {
  fun isAllergicTo(a: Allergen) = (allergies and a.score) == a.score

  fun getList() = Allergen.values().filter(::isAllergicTo)
}
