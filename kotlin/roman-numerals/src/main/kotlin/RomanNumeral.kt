object RomanNumeral {
  private val digits = listOf(
      "I" to "V",
      "X" to "L",
      "C" to "D",
      "M" to "?",
      "?" to "?")

  private fun convert(decimalPlace: Int, n: Int) =
      if (decimalPlace < 0 || decimalPlace > 3) emptyList()
      else {
        val (d0, d1) = digits[decimalPlace]
        val (d2, _) = digits[decimalPlace + 1]
        when (n) {
          in 0..3 -> Array(n) { d0 }.asList()
          4       -> listOf(d0, d1)
          5       -> listOf(d1)
          in 0..8 -> listOf(d1) + Array(n - 5) { d0 }.asList()
          9       -> listOf(d0, d2)
          else    -> emptyList()
        }
      }

  private fun decimal(n: Int): List<Int> = when(n) {
    0    -> emptyList()
    else -> listOf(n % 10) + decimal(n / 10)
  }

  fun value(n: Int) = decimal(n)
      .mapIndexed(::convert)
      .fold(emptyList<String>()) { acc, v -> v + acc }
      .joinToString("")
}
