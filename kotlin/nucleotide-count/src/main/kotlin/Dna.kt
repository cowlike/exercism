class Dna(val dna: String) {
  val nucleotideCounts = "AGCT".map { it to 0 }.toMap()
      .let { counts ->
        require(dna.all { it in counts.keys })
        counts + dna.groupingBy { it }.eachCount()
      }
}
