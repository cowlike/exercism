fun trans(nucleotide: Char) =
        when (nucleotide) {
            'G' -> 'C'
            'C' -> 'G'
            'T' -> 'A'
            else -> 'U'
        }

fun transcribeToRna(dna: String) =
    dna.fold("") { rna, nucleotide -> rna + trans(nucleotide.toUpperCase()) }
