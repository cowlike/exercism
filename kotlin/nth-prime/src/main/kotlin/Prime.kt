object Prime {
  fun nth(n: Int): Int {
    require(n > 0) {"There is no zeroth prime."}

    return primes().drop(n - 1).first()
  }
}

val primes = {
  var n = 2
  var acc: MutableList<Int> = mutableListOf()

  generateSequence {
    tailrec fun next(): Int {
      if (acc.none { n % it == 0 }) {
        acc.add(n)
        return n
      }
      else {
        n += 1
        return next()
      }
    }
    next()
  }
}