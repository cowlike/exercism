class Anagram(val input: String) {

  fun match(words: List<String>) = input.toLowerCase().let { w1 ->
    words.filter {
      it.toLowerCase().let { w2 ->
        w1 != w2 && w1.toList().sorted() == w2.toList().sorted()
      }
    }.toSet()
  }
}