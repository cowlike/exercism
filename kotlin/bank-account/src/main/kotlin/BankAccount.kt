import java.util.concurrent.atomic.*

class BankAccount() {
  private var isOpen = true
  private var bal = AtomicInteger(0)

  private fun apply(op: () -> Int) =
      if (isOpen) bal.run { op() } else throw IllegalStateException()

  var balance = 0
    get() = apply { bal.get() }

  fun adjustBalance(amount: Int) = apply { bal.addAndGet(amount) }

  fun close() { isOpen = false }
}
