class DiamondPrinter {
  private val letters = ('A'..'Z')

  private fun spaces(n: Int) = " ".repeat(n)

  private fun mkStr(c: Char, len: Int): String {
    val idx = letters.indexOf(c)
    val cStr =
        if (idx == 0) "$c"
        else "$c${spaces(2 * idx - 1)}$c"
    val padding = spaces((len - cStr.length) / 2)
    return "$padding$cStr$padding"
  }

  fun printToList(c: Char): List<String> {
    val cUpper = c.toUpperCase()
    require(cUpper in letters)

    val maxLen = 2 * letters.indexOf(cUpper) + 1
    val up = ('A'..cUpper).map { mkStr(it, maxLen) }
    return up + up.reversed().drop(1)
  }
}
