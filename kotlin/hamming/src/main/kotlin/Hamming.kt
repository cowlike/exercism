class Hamming {
    companion object {
        fun compute(xs: String, ys: String): Int {
            if (xs.length != ys.length) {
                throw IllegalArgumentException("left and right strands must be of equal length.")
            }

            return xs.zip(ys, Char::equals).filter {!it}.size
        }
    }
}