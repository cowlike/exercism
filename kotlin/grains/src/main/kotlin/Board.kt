import java.math.BigInteger

object Board {
  val limit = 64

  fun getGrainCountForSquare(n: Int): BigInteger {
    require(n in 1..limit) {
      "Only integers between 1 and 64 (inclusive) are allowed" }

    return grainCount(n - 1)
  }

  fun getTotalGrainCount() = grainCount().dec()

  private fun grainCount(n: Int = limit) = 2.toBigInteger().pow(n)
}
