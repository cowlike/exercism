import java.math.BigDecimal

class SpaceAge(val seconds: Long) {
    private val earthSecondsPerYear = 31_557_600.0

    private fun Double.round(places: Int) =
            BigDecimal(this).setScale(places, BigDecimal.ROUND_HALF_UP).toDouble()

    private fun toYears(factor: Double) =
            (this.seconds / earthSecondsPerYear / factor).round(2)

    fun onMercury() = toYears(0.240846)
    fun onVenus() = toYears(0.61519726)
    fun onEarth() = toYears(1.0)
    fun onMars() = toYears(1.8808158)
    fun onJupiter() = toYears(11.862615)
    fun onSaturn() = toYears(29.447498)
    fun onUranus() = toYears(84.016846)
    fun onNeptune() = toYears(164.79132)
}