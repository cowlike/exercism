class School {
  private var db = emptyMap<Int, List<String>>()

  fun add(name: String, grade: Int) {
    val names = db.getOrDefault(grade, emptyList())
    db = db + (grade to names + name)
  }

  fun db() = db

  fun grade(g: Int) = db.getOrDefault(g, emptyList())

  fun sort() = db.map { (k,v) -> k to v.sorted() }.toMap().toSortedMap()
}