class PhoneNumber(val input: String) {
  val number: String? = input
      .filter { it.isDigit() }
      .let {
        when (it.length) {
          10 -> result(it)
          11 -> if (it.first() == '1') result(it.drop(1)) else result()
          else -> result()
        }
      }

  private fun result(s: String = "000000"): String {
    require (s[0] !in "01" && s[3] !in "01")
    return s
  }
}
