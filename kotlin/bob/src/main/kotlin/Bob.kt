object Bob {
  fun hey(input: String) = input.trim().let { words ->
    when {
      words.isEmpty() -> "Fine. Be that way!"
      words.hasAlpha() && words.toUpperCase() == input -> "Whoa, chill out!"
      words.endsWith('?') -> "Sure."
      else -> "Whatever."
    }
  }

  private fun String.hasAlpha() = this.contains("[a-zA-z]".toRegex())
}
