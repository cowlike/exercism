package Acronym

fun generate(s:String) =
        s.split("""\W""".toRegex())
                .filterNot {it.isEmpty()}
                .joinToString("") {it.first().toString().toUpperCase()}

