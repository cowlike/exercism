object CollatzCalculator {

  fun computeStepCount(n: Int): Int {
    require (n > 0) {"Only natural numbers are allowed"}

    return generateSequence(n) { if (it % 2 == 0) it / 2 else 3 * it + 1 }
        .takeWhile { it > 1 }
        .count()
  }
}