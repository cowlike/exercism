object Isogram {
  fun isIsogram(input: String) =
      input.toLowerCase().filter { it.isLetter() }.let {
        it.length == it.asSequence().distinct().count()
      }
}
