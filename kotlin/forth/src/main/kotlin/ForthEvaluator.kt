import java.util.*

class ForthEvaluator {

  private class Func(val name: String, val args: Int, val wrapped: (Deque<Int>) -> Deque<Int>) {
    operator fun invoke(stack: Deque<Int>): Deque<Int> {
      require(stack.size >= args) {
        "$name requires that the stack contain at least $args value${if (args > 1) "s" else ""}"
      }
      return wrapped(stack)
    }
  }

  private fun missing(name: String) = Func(name, 0) { stack ->
    throw IllegalArgumentException("No definition available for operator \"${name}\"")
  }

  private val functionMap = mutableMapOf(
      "+" to Func("Addition", 2) { stack ->
        stack.push(stack.pop() + stack.pop())
        stack
      },
      "-" to Func("Subtraction", 2) { stack ->
        Pair(stack.pop(), stack.pop()).let { (y, x) ->
          stack.push(x - y)
          stack
        }
      },
      "*" to Func("Multiplication", 2) { stack ->
        stack.push(stack.pop() * stack.pop())
        stack
      },
      "/" to Func("Division", 2) { stack ->
        Pair(stack.pop(), stack.pop()).let { (divisor, dividend) ->
          require(divisor != 0) { "Division by 0 is not allowed" }
          stack.push(dividend / divisor)
          stack
        }
      },
      "DROP" to Func("Dropping", 1) { stack ->
        stack.pop()
        stack
      },
      "DUP" to Func("Duplicating", 1) { stack ->
        stack.push(stack.peek())
        stack
      },
      "SWAP" to Func("Swapping", 2) { stack ->
        Pair(stack.pop(), stack.pop()).let { (f, s) ->
          stack.push(f)
          stack.push(s)
          stack
        }
      },
      "OVER" to Func("Overing", 2) { stack ->
        Pair(stack.pop(), stack.pop()).let { (f, s) ->
          stack.push(s)
          stack.push(f)
          stack.push(s)
          stack
        }
      }
  )

  private val num = Regex("""-*\d+""")

  private fun addCommand(tokens: List<String>) {
    require(!num.matches(tokens.first())) { "Cannot redefine numbers" }

    functionMap[tokens.first().toUpperCase()] = Func(tokens.first(), 0) { stack ->
      run(stack, tokens.drop(1).dropLast(1))
    }
  }

  private fun run(stack: Deque<Int>, tokens: List<String>): Deque<Int> =
      if (tokens.first() == ":") {
        addCommand(tokens.drop(1))
        stack
      } else {
        tokens.fold(stack) { s, token: String ->
          when {
            num.matches(token) -> {
              s.push(Integer.parseInt(token)); s
            }
            else -> functionMap.getOrDefault(token.toUpperCase(), missing(token))(s)
          }
        }
      }

  fun evaluateProgram(lines: List<String>): List<Int> {
    fun tokenizeAndRun(stack: Deque<Int>, line: String) =
        run(stack, line.replace("\\s+".toRegex(), " ").split(' '))

    val d: Deque<Int> = LinkedList()
    lines.fold(d, ::tokenizeAndRun)
    return d.toList().reversed()
  }
}
