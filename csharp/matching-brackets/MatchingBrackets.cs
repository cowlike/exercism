using System;
using System.Collections.Generic;

public static class MatchingBrackets
{
    public static bool IsPaired(string input) {
        var matches = new Stack<(char L, char R)>();
        List<(char L, char R)> pairs = new() {
            (L:'{', R:'}'),
            (L:'[', R:']'),
            (L:'(', R:')'),
            (L:'"', R:'"')
        };

        var inQuote = false;

        foreach (var ch in input) {
            if (ch == '"') {
                if (inQuote) {
                    inQuote = false;
                }
                else {
                    inQuote = true;
                }
                continue;
            }
            else if (inQuote) {
                continue;
            }
            var pMatch = pairs.Find(p => ch == p.L || ch == p.R);
            if (pMatch.L == ch) {
                matches.Push(pMatch);
            }
            else if (pMatch.R == ch) {
                if (matches.Count == 0 || matches.Pop().L != pMatch.L) {
                return false;
                }
            }
        }

        return matches.Count == 0;
    }
}
