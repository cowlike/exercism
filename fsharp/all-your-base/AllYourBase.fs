module AllYourBase

type private OptionBuilder() =
    // member x.Bind(v,f) = Option.bind f v
    member x.Return v = Option.Some v
    // member x.ReturnFrom o = o
    member x.Zero() = Option.None

let private opt = OptionBuilder()

let private toVal numBase digits = List.fold (fun acc v -> acc * numBase + v) 0 digits

let private fromVal numBase num = 
    let rec convert acc b = function
        | 0 -> acc
        | n -> convert ((n % b) :: acc) b (n / b)
    if num = 0 then [0] else convert [] numBase num

let rebase inputBase inputDigits outputBase =
    opt {
        if inputBase > 1 &&
            outputBase > 1 &&
            (not << List.isEmpty) inputDigits &&
            List.forall (fun x -> x < inputBase && x >= 0) inputDigits 
        then
            return (fromVal outputBase (toVal inputBase inputDigits))
    }
