module Acronym

open System.Text.RegularExpressions

let matchVals ms = Seq.cast<Match> ms |> Seq.map (fun m -> m.Value)

let words s = Regex.Matches (s, "(\w)+") |> matchVals

let mixed s = 
    match (Regex.Matches (s, "[A-Z][a-z]+") |> matchVals) with
    | ms when Seq.isEmpty ms -> Seq.take 1 (s.ToUpper())
    | ms -> Seq.map Seq.head ms

let acronym phrase = words phrase |> Seq.collect mixed
