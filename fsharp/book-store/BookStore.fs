﻿module BookStore

let private price n =
    let discounts =
        [(1,0.0M); (2,0.05M); (3,0.10M); (4,0.20M); (5,0.25M)] |> Map.ofList
    let total = (decimal n) * 8.0M in total - total * discounts.[n]

let rec private byN bookGroups n =
    let uniqueBooks = List.length bookGroups
    let rmFirst n ys =
        let xs = List.sortByDescending (List.length << snd) ys
        List.take n xs
        |> List.map (fun (x,y) -> (x, List.tail y))
        |> List.filter (not << List.isEmpty << snd)
        |> List.append (List.skip n xs)

    if uniqueBooks < n then
        byN bookGroups uniqueBooks
    elif n >= 1 && n <= 5 then
        price n + byN (rmFirst n bookGroups) n
    else 0.0M

let total books =
    [5..-1..1]
    |> List.map (byN (List.groupBy id books))
    |> List.min

/// variation passing in books as a simple list
// let rec byN books n =
//     let bookGroups = List.groupBy id books
//     let uniqueBooks = List.length bookGroups
//     let rmFirst n ys =
//         let xs = List.sortByDescending (List.length << snd) ys
//         List.take n xs
//         |> List.collect (List.tail << snd)
//         |> List.append (List.skip n xs |> List.collect snd)

//     if uniqueBooks < n then
//         byN books uniqueBooks
//     elif n >= 1 && n <= 5 then
//         price n + byN (rmFirst n bookGroups) n
//     else 0.0M

// let total books =
//     [5..-1..1]
//     |> List.map (byN books)
//     |> List.min
