module Grains

let square n = pown 2I (n-1)

let total = square 65 - 1I