﻿module RunLengthEncoding

open System

let rec private runCount = function
    | [] -> []
    | x :: xs -> 
        let same = List.takeWhile ((=) x) xs
        let rest = List.skipWhile ((=) x) xs
        (1 + List.length same, x) :: runCount rest

let rec private decodeList chars =
    let toNum acc c = acc * 10 + (int c - int '0')
    match chars with
    | [] -> []
    | (x :: _) as s when Char.IsDigit x ->
        let n = List.takeWhile Char.IsDigit s |> List.fold toNum 0
        List.skipWhile Char.IsDigit s |> function
            | [] -> []
            | y :: ys -> String.replicate n (string y) :: decodeList ys
    | x :: xs -> string x :: decodeList xs

let encode input =
    let toStr (n, c) = if n = 1 then string c else sprintf "%d%c" n c
    input 
    |> List.ofSeq 
    |> runCount 
    |> List.fold (fun acc v -> acc + toStr v) ""

let decode input = String.Join("", input |> List.ofSeq |> decodeList)
