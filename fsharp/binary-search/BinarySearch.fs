module BinarySearch

let (|LeftHalf|RightHalf|Equals|) (value, midPoint, arr : array<'a>) = 
    if value > arr.[midPoint] then RightHalf 
    elif value < arr.[midPoint] then LeftHalf
    else Equals

let rec private search (input : array<'a>) value left right =
    if left > right then None
    else
        let mid = (left + right) / 2 
        match (value, mid, input) with
        | RightHalf -> search input value (mid + 1) right
        | LeftHalf -> search input value left (mid - 1)
        | Equals -> Some mid

let binarySearch input value = search input value 0 (Array.length input - 1)
