module AffineCipher

open System

let private coprime a b =
  let rec gcd a b =
    if a = b then a
    elif a > b then gcd (a - b) b
    else gcd a (b - a)

  gcd a b = 1

let private mmi a m =
  let rec loop a x m =
    match a * x % m with
    | 1 -> x
    | _ -> loop a (x + 1) m

  loop a 1 m

let private (%%) n m =
  match n % m with
  | mod' when sign mod' >= 0 -> mod'
  | mod' -> abs m + mod'

let private (|Letter|Digit|Other|) ch =
  if Char.IsLetter ch then Letter
  elif Char.IsDigit ch then Digit
  else Other

let private guard a =
  if not <| coprime a 26 then
    raise (ArgumentException $"{a} and 26 are not coprime")

let decode a b (cipherText: string) =
  // D(y) = (a^-1)(y - b) mod m
  let decrypt ch =
    match ch with
    | Letter -> (mmi a 26) * (int ch - 97 - b) %% 26 + 97 |> char
    | _ -> ch

  guard a

  cipherText.Replace(" ", "")
  |> Seq.map decrypt
  |> fun xs -> String.Join("", xs)

let encode a b (plainText: string) =
  // E(x) = (ai + b) mod m
  let encrypt ch =
    ((int ch - 97) * a + b) %% 26 + 97 |> char

  guard a

  Seq.foldBack
    (fun ch acc ->
      match ch with
      | Letter -> encrypt ch :: acc
      | Digit -> ch :: acc
      | _ -> acc)
    (plainText.ToLower())
    []
  |> List.chunkBySize 5
  |> List.map (fun xs -> String.Join("", xs))
  |> fun xs -> String.Join(" ", xs)
