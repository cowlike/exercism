module AffineCipher

open System

let alphabet = [ 'a' .. 'z' ]

let iff expr tExpr fExpr = if expr then tExpr else fExpr

let coprime a b =
  let rec gcd a b =
    if a = b then a
    elif a > b then gcd (a - b) b
    else gcd a (b - a)

  gcd a b = 1

let decode a b cipheredText =
  // D(y) = (a^-1)(y - b) mod m
  cipheredText

let strJoin ch (xs: string seq) = String.Join(ch, xs)

let encode a b (plainText: string) =
  // E(x) = (ai + b) mod m
  let encryptChar (ch: char) =
    match Char.IsDigit ch with
    | true -> ch
    | _ -> ((int ch - 97) * a + b) % 26 + 97 |> char

  if not <| coprime a 26 then
    raise
    <| ArgumentException $"{a} and {b} are not coprime"


  Seq.foldBack
    // (fun ch acc -> iff (Char.IsLetterOrDigit(ch)) (ch :: acc) acc)
    (fun ch acc ->
    if Char.IsLetterOrDigit(ch) then
      ch :: acc
    else
      acc)
    (plainText.ToLower())
    []
  |> List.chunkBySize 5
  |> List.map (fun xs -> List.fold (fun acc v -> acc + string v) "" xs)
  |> strJoin " "

// let encode a b (plainText: string) =
//   // E(x) = (ai + b) mod m
//   let encryptChar (ch: char) =
//     if Char.IsDigit ch then
//       ch
//     else
//       ((int ch - 97) * a + b) % 26 + 97 |> char

//   if not <| coprime a b then
//     raise
//     <| ArgumentException $"{a} and {b} are not coprime"


//   Seq.foldBack
//     (fun ch acc ->
//       if Char.IsLetterOrDigit(ch) then
//         (ch |> encryptChar |> string) + acc
//       else
//         acc)
//     (plainText.ToLower())
//     ""
