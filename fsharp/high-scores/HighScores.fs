﻿module HighScores

let scores = id

let latest = List.last

let highest = List.max

let top : (int list -> int list) = 
    Seq.sortDescending >> Seq.truncate 3 >> List.ofSeq

let report values =
    let max = highest values
    let last = latest values
    let suffix =
        if last >= max then "your personal best!"
        else sprintf "%d short of your personal best!" (max - last)

    sprintf "Your latest score was %d. That's %s" last suffix
