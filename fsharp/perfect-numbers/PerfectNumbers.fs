module PerfectNumbers

type NumberType = Deficient | Abundant | Perfect

let classify n =
    let factors n = 1 :: List.filter (fun x -> n % x = 0) [2..n/2]

    match factors n |> List.sum with
    | x when x < n -> Deficient
    | x when x > n -> Abundant 
    | _ -> Perfect
