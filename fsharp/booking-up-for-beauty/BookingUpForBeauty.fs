module BookingUpForBeauty

open System

let schedule (appointmentDateDescription: string) : DateTime =
  DateTime.Parse(appointmentDateDescription)

let hasPassed (appointmentDate: DateTime) : bool = appointmentDate < DateTime.Now

let isAfternoonAppointment (appointmentDate: DateTime) : bool =
  appointmentDate.TimeOfDay >= TimeSpan(12, 0, 0)
  && appointmentDate.TimeOfDay < TimeSpan(18, 0, 0)

let description (appointmentDate: DateTime) : string =
  $"You have an appointment on {appointmentDate.ToString()}."

let anniversaryDate () : DateTime =
  let currentYear = DateTime.Now.Year
  let anniversary = DateTime(currentYear, 9, 15, 0, 0, 0)

  if hasPassed anniversary then
    DateTime(currentYear + 1, 9, 15, 0, 0, 0)
  else
    anniversary
