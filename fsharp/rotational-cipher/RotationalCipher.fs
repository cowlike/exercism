﻿module RotationalCipher

open System

let private rotateFrom key start c =
    (int c - int start + key) % 26 + (int start) |> char

let rotate shiftKey text =
    let rot = rotateFrom shiftKey
    text
    |> Seq.map (fun c -> 
        if Char.IsLower(c) then rot 'a' c
        elif Char.IsUpper(c) then rot 'A' c
        else c
        |> string)
    |> String.concat ""
