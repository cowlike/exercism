module SecretHandshake

let handshake n = 
    if n < 0 || n > 31 then []
    else
        let moves = ["wink"; "double blink"; "close your eyes"; "jump"]
        let bitOn n bit = (n &&& (1 <<< bit)) <> 0
        let addMove moves move test = if test then move :: moves else moves
        
        List.fold2 addMove [] moves (List.map (bitOn n) [0..3])
        |> if bitOn n 4 then id else List.rev
