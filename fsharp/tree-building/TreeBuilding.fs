﻿module TreeBuilding

type Record = { RecordId: int; ParentId: int }

type Tree<'a> = Tree of 'a * 'a Tree list

let (>>=) m f = Option.bind f m

let isLeaf (Tree (_, ch)) = List.isEmpty ch

let isBranch t = not <| isLeaf t

let value (Tree (v, _)) = v

let children (Tree (_, ch)) = ch

let childOf tree v = 
    children tree
    |> List.map value
    |> List.contains v

let rec parent v tree =
    match tree with
    | Tree (_, []) -> None
    | n when childOf tree v -> Some n
    | Tree (_, xs) -> List.map (parent v) xs |> List.tryPick id

let node tree v =
    if childOf tree v then Some tree else parent v tree
        >>= (children >> Some) 
        >>= List.tryFind (value >> ((=) v))

let prune tree v = tree //WORK

let rec add v p tree =
    match tree with
    | Tree (p', []) as n when p <> p' -> n
    | Tree (p', ch) when p = p' -> Tree (p', Tree (v,[]) :: ch)
    | Tree (p', ch) -> Tree (p', List.map (add v p) ch)

let valid = function
    | [] -> false
    | x::xs -> 
        x.RecordId = 0 && x.ParentId = 0 
        && List.forall2 (fun x y -> x.RecordId = y+1 && x.ParentId < x.RecordId) xs [0..(List.length xs) - 1]

let buildTree records = 
    let sorted = List.sortBy (fun r -> r.RecordId) records
    if valid sorted then
        (List.tail sorted)
        |> List.fold (fun acc v -> add v.RecordId v.ParentId acc) (Tree (0, [])) 
        |> Some
    else 
        None
