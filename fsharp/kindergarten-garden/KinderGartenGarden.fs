module KinderGartenGarden

type Plant = Grass | Violets | Clover | Radishes | NOPLANT

let private toPlant =
    function
    | 'G' -> Grass 
    | 'V' -> Violets 
    | 'C' -> Clover 
    | 'R' -> Radishes
    | _ -> NOPLANT

let convertRow row = 
    row |> List.ofSeq |> List.map toPlant |> Seq.chunkBySize 2 

let garden students (input : string) =
    input.Split([|'\n'|])
    |> Array.map (Seq.zip (List.sort students) << convertRow)
    |> Seq.fold Seq.append Seq.empty
    

let defaultGarden input =
    let defStudents = [ "Alice"; "Bob"; "Charlie"; "David";
                        "Eve"; "Fred"; "Ginny"; "Harriet";
                        "Ileana"; "Joseph"; "Kincaid"; "Larry" ]
    garden defStudents input

let lookupPlants name garden =
    Seq.filter (fst >> ((=) name)) garden
    |> Seq.fold (fun acc (_,v) -> Array.append acc v) [||]
    |> List.ofSeq
