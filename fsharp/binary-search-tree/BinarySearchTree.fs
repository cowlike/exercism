module BinarySearchTree

(*
    I realize this design could be implemented with the
    left and right subtrees being optional but I don't like
    tying that optionality into the tree type. It prevents 
    you from creating an empty tree. I'd rather return an 
    optional value from the "value" function rather than 
    throw an exception but that's not what is tested.
*)

type Tree<'a> = Nil | Node of 'a * Tree<'a> * Tree<'a>

let singleton x = Node (x, Nil, Nil)

let value = function
    | Node (v,_,_) -> v
    | _ -> failwith "Empty tree has no value"

let rec insert x = function
    | Node (v, l, r) -> if x <= v 
                        then Node (v, insert x l, r) 
                        else Node (v, l, insert x r) 
    | _ -> Node (x, Nil, Nil)

let node = function
    | Nil -> None
    | n -> Some n

let left = function
    | Node (_, l, _) -> node l
    | _ -> None

let right = function
    | Node (_, _, r) -> node r
    | _ -> None

let flip f x y = f y x

let fromList lst = List.fold (flip insert) Nil lst

let rec toList = function
    | Nil -> []
    | Node (v, Nil, Nil) -> [v]
    | Node (v, l, r) -> toList l @ [v] @ toList r
