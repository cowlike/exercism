module RobotName

let mkName =
  let randMix seed (lst : 'a list) =
    let gen = System.Random(seed)
    let maxVal = List.length lst
    Seq.initInfinite (fun _ -> lst.[gen.Next(maxVal)])
  
  let chars = randMix 0 ['A'..'Z']
  let nums = randMix 1 ['0'..'9']

  (fun () -> 
    (Seq.take 2 chars, Seq.take 3 nums)
      ||> Seq.append
      |> Seq.map string
      |> System.String.Concat)
  
let mkRobot() = mkName()

let name = id

let reset _ = mkName()
