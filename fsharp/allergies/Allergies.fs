module Allergies

type Allergen = 
    Eggs | Peanuts | Shellfish | Strawberries | Tomatoes | Chocolate | Pollen | Cats

let private allergens = 
    [|Eggs; Peanuts; Shellfish; Strawberries; Tomatoes; Chocolate; Pollen; Cats|]

let private flip f x y = f y x

let allergicTo allergen n =
    Array.findIndex ((=) allergen) allergens
    |> ((<<<) 1 >> (&&&) n >> (<>) 0)

let allergies n = Array.filter (flip allergicTo n) allergens
