﻿module HelloWorldTest

open FsUnit.Xunit
open Xunit

open HelloWorld

[<Fact>]
let ``No name`` () =
    hello None |> should equal "Hello, World!"


[<Fact>]
let ``Sample name`` () =
    hello (Some "Alice") |> should equal "Hello, Alice!"

[<Fact>]
let ``Other sample name`` () =
    hello (Some "Bob") |> should equal "Hello, Bob!"
