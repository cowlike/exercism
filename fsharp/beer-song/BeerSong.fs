module BeerSong

open System

let flip f a b = f b a

let bottle n =
  match n with
  | 0 -> "no more bottles"
  | _ -> sprintf "%d bottle%s" n (if n > 1 then "s" else "")

let capitalize (s:string) = match s with
  | "" -> ""
  | _ -> (string (Char.ToUpper s.[0])) + (s.Substring 1)

let verse n =
  let prefix = sprintf "%s of beer on the wall, %s of beer.\n"
                 (capitalize (bottle n)) (bottle n)
  match n with
  | 0 -> sprintf "%s\n%s\n"
           "No more bottles of beer on the wall, no more bottles of beer."
           "Go to the store and buy some more, 99 bottles of beer on the wall."
  | _ -> sprintf "%sTake %s down and pass it around, %s of beer on the wall.\n"
           prefix (if n = 1 then "it" else "one") (bottle (n - 1))

let verses s e =
  [for n in [s .. -1 .. e] do yield verse n]
  |> String.concat "\n"
  |> flip (+) "\n"

let sing = verses 99 0
