module ParallelLetterFrequency

open System

let private mergeMap f m mAcc = 
    Map.foldBack 
        (fun k v acc -> 
            match Option.map (f v) (Map.tryFind k acc) with 
            | (Some n) -> Map.add k n acc 
            | None -> Map.add k v acc) m mAcc

let private toMap (xs:string) =
    xs.ToLower()
    |> Seq.filter Char.IsLetter
    |> Array.ofSeq
    |> Array.groupBy id
    |> Array.map (fun (k,v) -> (k, Array.length v))
    |> Map.ofArray

let frequency = function
    | [] -> Map.empty
    | xs -> Array.ofList xs
            |> Array.Parallel.map toMap
            |> Array.reduce (fun acc m -> mergeMap (+) m acc)
