module Sieve

let rec sieve' = function
    | [] -> []
    | (x :: xs) -> x :: sieve' (List.filter (fun y -> y % x <> 0) xs)

let primesUpTo' n = sieve' [2..n]

let rec sieve value = seq { 
  yield value
  yield! Seq.filter (fun x -> x % value <> 0) (sieve (value + 1)) }

let primesUpTo n = 
    sieve 2 
    |> Seq.takeWhile ((>=) n)
    |> List.ofSeq