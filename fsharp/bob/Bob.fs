module Bob

open System

// let isAlpha (input:string) = String.exists Char.IsLetter input

// let (|Yell|_|) input = if isAlpha input && input = input.ToUpper() 
//                        then Some(input) else None

// let (|Empty|_|) (input:string) = if input.Trim() = "" then Some(input) else None

// let hey (say:string) =
//   match say with
//   | Empty _ -> "Fine. Be that way!"
//   | Yell _ -> "Whoa, chill out!"
//   | q when q.EndsWith("?") -> "Sure."
//   | _ -> "Whatever."

let private isAlpha = String.exists Char.IsLetter 

let private isQuestion (input: string) = input.Trim().EndsWith("?")

let private (|Yell|YellQuestion|Question|Default|) input = 
    if isAlpha input && input = input.ToUpper() then
        if isQuestion input then YellQuestion else Yell
    elif isQuestion input then Question
    else Default

let private (|Empty|_|) (input:string) = if input.Trim() = "" then Some(input) else None

let response (say:string) =
    match say with
    | Empty _ -> "Fine. Be that way!"
    | Yell -> "Whoa, chill out!"
    | YellQuestion -> "Calm down, I know what I'm doing!" 
    | Question -> "Sure."
    | _ -> "Whatever."
