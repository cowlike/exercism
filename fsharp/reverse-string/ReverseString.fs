﻿module ReverseString

let reverse (input: string) =
    input
    |> Seq.fold (fun acc c -> (string c) :: acc) []
    |> String.concat ""
