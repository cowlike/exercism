module Matrix

type Matrix = Matrix of array<array<int>>

let private aCons x xs = Array.append [|x|] xs

let rec private transpose = function
    | m when not (Array.isEmpty (Array.head m)) -> 
        aCons (Array.map Array.head m) (transpose (Array.map Array.tail m))
    | _ -> [||]

let fromString (matrix:string) =
    matrix.Split([|'\n'|])
    |> Array.map (fun s -> Array.map int (s.Split([|' '|])))
    |> Matrix

let rows (Matrix m) = m

let cols (Matrix m) = transpose m 
