module TwelveDaysSong

let private lines =
    [|
        ("first", "a Partridge in a Pear Tree");
        ("second", "two Turtle Doves");
        ("third", "three French Hens");
        ("fourth", "four Calling Birds");
        ("fifth", "five Gold Rings");
        ("sixth", "six Geese-a-Laying");
        ("seventh", "seven Swans-a-Swimming");
        ("eighth", "eight Maids-a-Milking");
        ("ninth", "nine Ladies Dancing");
        ("tenth", "ten Lords-a-Leaping");
        ("eleventh", "eleven Pipers Piping");
        ("twelfth", "twelve Drummers Drumming");
    |]

let private fullLine n = 
    lines.[n]
    ||> sprintf "On the %s day of Christmas my true love gave to me, %s"

let private partLine initial n = initial + (if n = 0 then ", and " else ", ") + snd lines.[n]

let verse n = 
    match n - 1 with
    | 0 -> fullLine 0
    | v when v >= 1 && v < 12 -> List.fold partLine (fullLine v) [v-1 .. -1 .. 0]
    | _ -> ""
    + ".\n"

let verses v0 vn = List.fold (fun acc n -> acc + verse n + "\n") "" [v0 .. vn]

let song = verses 1 12