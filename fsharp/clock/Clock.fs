module Clock

type Clock = Clock of int * int

let private normalize hour minute =
    let modulo x y = ((x % y) + y) % y
    ((hour + (minute / 60)) % 24, modulo minute 60)

let mkClock h m = Clock (normalize h m)

let private adjust m (Clock (h0, m0)) =
    let h = if m < 0 && h0 = 0 then 24 else h0
    mkClock 0 (h * 60 + m0 + m)

let add = adjust

let subtract m = adjust (-m)

let display (Clock (h, m)) = sprintf "%02d:%02d" h m
