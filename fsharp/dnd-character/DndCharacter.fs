﻿module DndCharacter

open System

let rolls =
    let r = Random()
    Seq.initInfinite (fun _ -> r.Next(1,7))

let modifier x = (x - 10 |> float) / 2.0 |> floor |> int

let ability() = Seq.take 4 rolls |> Seq.sort |> Seq.skip 1 |> Seq.sum

type DndCharacter() =
    let strength = ability()
    let dexterity = ability()
    let constitution = ability()
    let intelligence = ability()
    let wisdom = ability()
    let charisma = ability()
    let hitpoints = 10 + modifier constitution

    member __.Strength with get() = strength
    member __.Dexterity with get() = dexterity
    member __.Constitution with get() = constitution
    member __.Intelligence with get() = intelligence
    member __.Wisdom with get() = wisdom
    member __.Charisma with get() = charisma
    member __.Hitpoints with get() = hitpoints
