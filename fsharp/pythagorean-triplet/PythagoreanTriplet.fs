module PythagoreanTriplet

let triplet a b c = 
    let [x;y;z] = List.sort [a;b;c]
    x,y,z

let isPythagorean (a,b,c) = a*a + b*b = c*c

let pythagoreanTriplets nFrom nTo =
    [ for a in [nFrom .. nTo] do 
      for b in [a .. nTo] do 
      for c in [b .. nTo] do 
        if isPythagorean (a,b,c) then yield (triplet a b c) ]
