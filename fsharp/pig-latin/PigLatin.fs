﻿module PigLatin

// let indexOfVowel (str: string) =
//   let f = if str.StartsWith('y') then ((+) 1) else id
//   Seq.tryFindIndex (fun c -> Seq.contains c "aeiouy") str |> Option.map f

// let startsWithVowel (str: string) =
//   indexOfVowel str = Some 0 || str.StartsWith("xr") || str.StartsWith("yt")

// let piggify = function
//   | str when startsWithVowel str -> str + "ay"
//   | str when (str.StartsWith("qu")) -> str.Substring(2) + "quay"
//   | str when String.length str = 1 -> str + "ay"
//   | str ->
//       let vloc = Option.defaultValue 0 (indexOfVowel str)
//       str.Substring(vloc) + str.Substring(0,vloc) + "ay"

// let translate (str: string) =
//   let words = str.Split([|' '|]) |> Array.map (piggify)
//   System.String.Join(' ', words)

// translate "abc de fgh"
// Seq.findIndex (fun c -> Seq.contains c "aeiou") "dddeggg"


open System.Text.RegularExpressions

let vowels = "aeiou"
let quPattern = @"(^qu|^.qu)(.*)"
let initialVowelPattern = sprintf @"(^[%s]|^y[^%s]).*" vowels vowels
let nextVowelPattern = sprintf @"(.[^y%s]*)(.*)" vowels
let matches s pattern = Regex.Match(s, pattern).Success

let matchGroups s pattern =
    Regex.Match(s, pattern).Groups
    |> Seq.cast<Group>
    |> Seq.map (fun m -> m.Value)
    |> Array.ofSeq

let translateWord = function
    | s when matches s initialVowelPattern -> s + "ay"
    | s when matches s quPattern ->
        let groups = matchGroups s quPattern
        groups.[2] + groups.[1] + "ay"
    | s ->
        let groups = matchGroups s nextVowelPattern
        let suffix = groups.[2]
        if suffix = "ay" then groups.[0] + "ay"
        else suffix + groups.[1] + "ay"

let translate (s: string) =
    System.String.Join(" ", s.Split [|' '|] |> Array.map translateWord)
