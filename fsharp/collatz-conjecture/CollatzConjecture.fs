module CollatzConjecture

let private next =
  function
  | 1 -> None
  | x when x % 2 = 0 -> Some(x, x / 2)
  | x -> Some(x, x * 3 + 1)

let private collLen = Seq.unfold next >> Seq.length

let steps x =
  if x < 1 then
    None
  else
    collLen x |> Some
