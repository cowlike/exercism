open System

module Collatz =
    let private next = function
        | 1 -> None
        | x when x % 2 = 0 -> Some (x, x / 2)
        | x -> Some (x, x * 3 + 1)

    let private collLen = Seq.unfold next >> Seq.length

    let steps x = if x < 1 then None else collLen x |> Some

module ArmstrongNumbers =
    let private digits n =
        let rec digits' xs x = 
            if x < 10 then x :: xs 
            else digits' ((x % 10) :: xs) (x / 10)
        digits' [] n

    let isArmstrongNumber number =
        let xs = digits number
        List.sumBy (fun x -> pown x <| List.length xs) xs = number

module PigLatin =
    let vowels = ['a'; 'e'; 'i'; 'o'; 'u']

    let translate input = input + ""