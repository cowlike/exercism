module Gigasecond

open System

let gigasecond (dt:DateTime) = dt.AddSeconds(1000000000.0).Date