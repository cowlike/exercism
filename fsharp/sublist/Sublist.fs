module Sublist

type ListTypes = Equal | Unequal | Sublist | Superlist

let isSublist xs ys = 
    let lxs = List.length xs
    if lxs > List.length ys 
    then false
    else lxs = 0 || List.windowed lxs ys |> List.exists ((=) xs)

let sublist xs ys = 
    if xs = ys then Equal
    elif isSublist xs ys then Sublist
    elif isSublist ys xs then Superlist
    else Unequal