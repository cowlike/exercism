module ProteinTranslation

let codons =
  [ ("AUG","Methionine");
    ("UUU","Phenylalanine");
    ("UUC","Phenylalanine");
    ("UUA","Leucine");
    ("UUG","Leucine");
    ("UCU","Serine");
    ("UCC","Serine");
    ("UCA","Serine");
    ("UCG","Serine");
    ("UAU","Tyrosine");
    ("UAC","Tyrosine");
    ("UGU","Cysteine");
    ("UGC","Cysteine");
    ("UGG","Tryptophan");
    ("UAA","");
    ("UAG","");
    ("UGA","");
  ] |> Map.ofList

let private partBy n str =
  let rec pb n str acc = 
    match str with
    | "" -> List.rev acc
    | _ when str.Length < 3 -> List.rev (str :: acc)
    | _ -> pb n (str.Substring(3)) (str.Substring(0,3) :: acc) 
  pb n str []

let translate strand =
  partBy 3 strand
  |> List.takeWhile (fun v -> "" <> codons.[v])
  |> List.fold (fun acc v -> codons.[v] :: acc) []
  |> List.rev
