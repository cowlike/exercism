module Bowling

type Frame = Strike | Spare of int * int | Open of int * int | Partial of int | Illegal

let mkRollOne n = 
    if n = 10 then Strike
    else if n >= 0 && n < 10 then Partial n
    else Illegal

let mkRollTwo n = function
    | Partial x when n >= 0 && x + n = 10 -> Spare (x, n)
    | Partial x when n >= 0 && x + n < 10 -> Open (x, n)
    | _ -> Illegal

let rec getRolls n = function
    | [] -> []
    | _ when n <= 0 -> []
    | Strike :: xs -> 10 :: getRolls (n-1) xs
    | Spare (x, y) :: xs -> if n=1 then [x] else x :: y :: getRolls (n-2) xs
    | Open  (x, y) :: xs -> if n=1 then [x] else x :: y :: getRolls (n-2) xs
    | Partial x :: xs -> x :: getRolls (n-1) xs
    | _ :: xs -> getRolls (n-1) xs

let validGame xs = 
    let allValid = not << (List.contains Illegal)
    let len = List.length xs
    let bonusRolls = List.skip 10 >> getRolls 3 >> List.length
    len >= 10 && len <= 12 &&
        match xs |> List.skip 9 |> List.head with
            | Strike _ when bonusRolls xs = 2 -> allValid xs
            | Spare _ when bonusRolls xs = 1 -> allValid xs
            | Open _ when len = 10 -> allValid xs
            | _ -> false

let newGame = []

let roll pins game =
    let butLast = function 
        | [] -> [] 
        | xs -> List.take (List.length xs - 1) xs

    if List.isEmpty game then 
        List.append game [mkRollOne pins]
    else
        match List.last game with
        | Partial _ as frame -> List.append (butLast game) [mkRollTwo pins frame]
        | _ -> List.append game [mkRollOne pins]

let rec addScore n = function
    | [] -> 0
    | _ when n = 0 -> 0
    | Strike :: xs -> 10 + (getRolls 2 xs |> List.sum) + addScore (n-1) xs
    | Spare _ :: xs -> 10 + (getRolls 1 xs |> List.sum) + addScore (n-1) xs
    | Open (x, y) :: xs -> x + y + addScore (n-1) xs
    | Partial x :: xs -> x + addScore (n-1) xs
    | _ :: xs -> addScore (n-1) xs

let score xs = if validGame xs then (xs |> addScore 10 |> Some) else None
