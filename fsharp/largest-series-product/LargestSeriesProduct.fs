module LargestSeriesProduct

let largestProduct (digits:string) len =
    let product xs = Seq.fold (*) 1 xs

    if len = 0 then 1 
    else
        Seq.map (fun c -> int c - int '0') digits
        |> Seq.windowed len
        |> Seq.map product
        |> Seq.max
