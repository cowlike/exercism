module SumOfMultiples

let sum nums n =
    let isMult nums n = List.exists (fun x -> n % x = 0) nums

    [1 .. n - 1]
    |> List.filter (isMult nums)
    |> List.sum
