module CustomSet

type 'a Set = Set of 'a list with
    static member Equals (Set a1, Set a2) = a1 = a2 

let flip f x y = f y x

let empty = Set []

let isEmpty = function Set [] -> true | _ -> false

let singleton v = Set [v]

let contains v (Set lst) = List.contains v lst

let fromList lst = List.distinct lst |> List.sort |> Set

let isSubsetOf (Set left) (Set right) = List.forall (flip List.contains right) left

let intersection (Set left) (Set right) = 
    List.fold (fun acc v -> if List.contains v right then v::acc else acc) [] left
    |> fromList

let isDisjointFrom l r = intersection l r |> isEmpty

let insert v ((Set lst) as s) = 
    if contains v s then s else Set (v::lst |> List.sort)

let difference (Set left) (Set right) = 
    List.filter (fun v -> not <| List.contains v right) left |> Set

let union (Set left) (Set right) = left @ right |> fromList
