module PascalsTriangle

let tri = function
    | [] -> [[1]]
    | prev::_ as acc ->
        let next = prev |> List.windowed 2 |> List.map List.sum
        (1 :: next @ [1]) :: acc

let triangle n = 
    Seq.unfold (fun acc -> let x = tri acc in Some (Seq.head x, x)) [] 
    |> Seq.take n
    |> List.ofSeq
