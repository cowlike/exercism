﻿module PalindromeProducts

let isPalindrome n = 
    let rec rev acc = function
        | 0 -> acc
        | x -> rev (acc * 10 + (x % 10)) (x / 10)
    n = rev 0 n
    
let combinations a b = 
    [for x in [a .. b] do 
        for y in [x .. b] do 
        if (isPalindrome (x * y)) then yield (x * y, (x, y))]

let findPalindrome a b f = 
    match combinations a b with
    | [] -> None
    | xs ->
        let (m,_) = f xs
        (m, List.filter (fst >> ((=) m)) xs |> List.map snd)
        |> Some

let largest a b = findPalindrome a b List.max

let smallest a b = findPalindrome a b List.min
