module Palindrome

let isPalindrome n = 
    let rec rev acc = function
        | 0 -> acc
        | x -> rev (acc * 10 + (x % 10)) (x / 10)
    n = rev 0 n
    
let combinations a b = 
    [for x in [a .. b] do 
        for y in [x .. b] do 
        if (isPalindrome (x * y)) then yield (x * y, (x, y))]

let findPalindrome a b f = 
    let c = combinations a b 
    let m = fst (f c)
    (m, List.filter (fst >> ((=) m)) c |> List.map snd)

let largestPalindrome a b = findPalindrome a b List.max

let smallestPalindrome a b = findPalindrome a b List.min