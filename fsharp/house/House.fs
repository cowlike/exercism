module House

let private phrases = [
    ("house that Jack built.", "");
    ("malt", "lay in");
    ("rat", "ate");
    ("cat", "killed");
    ("dog", "worried");
    ("cow with the crumpled horn", "tossed");
    ("maiden all forlorn", "milked");
    ("man all tattered and torn", "kissed");
    ("priest all shaven and shorn", "married");
    ("rooster that crowed in the morn", "woke");
    ("farmer sowing his corn", "kept");
    ("horse and the hound and the horn", "belonged to");
    ]

let private buildVerse (s1,s2) v =
    let line = function
        | "" -> ""
        | s -> "\nthat " + s
    v + " the " + s1 + (line s2)

let private verse ps = function
    | n when n > 0 && n < 13 -> List.foldBack buildVerse (List.take n ps) "This is"
    | _ -> ""

let rhyme = 
    List.map (verse phrases) [1..12] 
    |> String.concat "\n\n"
