module Triangle

type TriangleKind = Equilateral | Isosceles | Scalene | Illogical

//I would rather return Illogical rather than throw an exception!

let kind a b c =
  let tri sides =
    match sides with
    | [a; b; c] when a + b <= c || a = 0m -> invalidOp "Invalid"
    | [a; b; c] when a = b && b = c -> TriangleKind.Equilateral
    | [a; b; c] when a = b || b = c -> TriangleKind.Isosceles
    | _ -> TriangleKind.Scalene
  tri (List.sort [a; b; c])
