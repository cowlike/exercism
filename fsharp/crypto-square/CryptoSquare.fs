module CryptoSquare

open System

let safeTail = function [] -> [] | (x::xs) -> xs
let safeHead = function [] -> None | (x::xs) -> Some x

let rec transpose = function
    | (_::_)::_ as m -> List.map safeHead m :: transpose (List.map safeTail m)
    | _ -> []

let roundSqrt = double >> sqrt >> ceil >> int

let join ch (lst: 'a list) = String.Join(ch, lst)

let normalizePlaintext =
    let toStr c = if Char.IsLetterOrDigit(c) then string (Char.ToLower(c)) else ""
    String.collect toStr

let size = normalizePlaintext >> String.length >> roundSqrt

let mkSegs s = 
    let s' = normalizePlaintext s
    let cols = String.length s' |> roundSqrt
    List.chunkBySize cols (List.ofSeq s')

let plaintextSegments = mkSegs >> List.map (join "")

let mkCipherSegs s =
    mkSegs s
    |> transpose
    |> List.map (List.choose id)
    |> List.map (join "")

let ciphertext = mkCipherSegs >> join ""

let normalizeCiphertext = mkCipherSegs >> join " "
