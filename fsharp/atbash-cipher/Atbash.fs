module Atbash

open System

let private subst code = function
    | c when Char.IsLetter(c) -> Map.find c code |> string
    | c when Char.IsDigit(c) -> string c
    | _ -> ""

let encode (s:string) = 
    let letters = ['a'..'z']
    let code = List.zip letters (List.rev letters) |> Map.ofList
    let mkString = List.fold (fun acc v -> acc + string v) ""

    s.ToLower()
    |> String.collect (subst code)
    |> List.ofSeq
    |> List.chunkBySize 5
    |> List.map mkString
    |> (fun l -> String.Join(" ", l))
