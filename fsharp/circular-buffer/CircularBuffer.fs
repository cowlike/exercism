module CircularBuffer

type 'a CircularBuffer = { 
    contents: 'a array; 
    size: int; 
    count: int;
    head: int; 
    tail: int 
}

let mkCircularBuffer sz = { 
    contents = Array.zeroCreate sz; 
    size = sz; 
    count = 0;
    head = 0; 
    tail = 0 
}

type ValueIndex = H | T

let inc b = function
    | H -> (b.head + 1) % b.size
    | T -> (b.tail + 1) % b.size

let read b = 
    if b.count = 0
    then invalidArg "b" "cannot read empty buffer"
    else b.contents.[b.tail], {b with tail = inc b T; count = b.count - 1}

let clear b = mkCircularBuffer b.size

let write v b =
    if b.count >= b.size
    then invalidArg "b" "buffer is full"
    else Array.set b.contents b.head v
         {b with head = inc b H; count = b.count + 1}

let forceWrite v b = 
    Array.set b.contents b.head v
    if b.count < b.size
    then {b with head = inc b H; count = b.count + 1}
    else {b with head = inc b H; tail = inc b T}
