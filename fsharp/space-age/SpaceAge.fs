module SpaceAge

open System

type Planet = Mercury | Venus | Earth | Mars | Jupiter | Saturn | Uranus | Neptune

let spaceAge planet seconds =
    let earthSecondsPerYear = 31557600m
    let toYears factor = Math.Round(seconds / earthSecondsPerYear / factor, 2)

    match planet with
    | Earth -> toYears 1.0m
    | Mercury -> toYears 0.2408467m
    | Venus -> toYears 0.61519726m
    | Mars -> toYears 1.8808158m
    | Jupiter -> toYears 11.862615m
    | Saturn -> toYears 29.447498m
    | Uranus -> toYears 84.016846m
    | Neptune -> toYears 164.79132m
