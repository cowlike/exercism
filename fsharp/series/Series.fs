module Series

let slices (input:string) len =
    let digitToNum d = int d - int '0'

    if input.Length >= len then input else failwith "window bigger than string"  
    |> Seq.map digitToNum
    |> Seq.windowed len
