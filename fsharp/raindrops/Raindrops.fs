module Raindrops

let convert n =
  let divisors = [(3,"Pling"); (5,"Plang"); (7,"Plong")]
  let divBy n (by, str) = if (n % by = 0) then str else ""

  match String.concat "" (List.map (divBy n) divisors) with
  | "" -> string n
  | result -> result
