module FoodChain

let private s = [| 
    ("fly", "I don't know why she swallowed the fly. Perhaps she'll die."); 
    ("spider", "It wriggled and jiggled and tickled inside her."); 
    ("bird", "How absurd to swallow a bird!"); 
    ("cat", "Imagine that, to swallow a cat!"); 
    ("dog", "What a hog, to swallow a dog!"); 
    ("goat", "Just opened her throat and swallowed a goat!"); 
    ("cow", "I don't know how she swallowed a cow!"); 
    ("horse", "She's dead, of course!") 
    |]

let private verseLead n = 
    sprintf "I know an old lady who swallowed a %s.\n%s" (fst s.[n-1]) (snd s.[n-1])

let rec private versePrior = 
    function
    | 1 -> snd s.[0]
    | 3 -> "She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.\n"
             + versePrior 2
    | n -> sprintf "She swallowed the %s to catch the %s.\n" (fst s.[n-1]) (fst s.[n-2])
             + versePrior (n-1)

let rec verse n = 
    match n with
    | x when x = 1 || x = 8 -> verseLead n
    | x when x > 1 && x < 8 -> verseLead n + "\n" + versePrior n
    | _                     -> ""

let song = List.reduceBack (fun acc v -> acc + "\n\n" + v) (List.map verse [1..8])