module Phrase

open System.Text.RegularExpressions

let wordCount (phrase : string) =
    Regex.Matches(phrase.ToLower(), "([a-z]+[\'][a-z]+|[a-z0-9]+)")
    |> Seq.cast<Match>
    |> Seq.groupBy (fun m -> m.Value)
    |> Seq.fold (fun acc (k, lst) -> Map.add k (Seq.length lst) acc) Map.empty
