﻿module Ledger

open System
open System.Globalization

type Locale = US | NL
type Currency = USD | EUR
[<NoComparison>]
[<NoEquality>]
type Resource = { 
    dateFormat: string;
    title: string;
    culture: CultureInfo
    neg: float -> float
    showNegative: float -> string -> string
    }

let getLocale (input: string) = if input.ToLower() = "nl-nl" then NL else US

let getCurrency input = if input = "EUR" then EUR else USD

let resources = 
    [ US, {dateFormat   = "MM\/dd\/yyyy"; 
           title        = "Date       | Description               | Change       "; 
           culture      = CultureInfo("en-US");
           neg          = fun c -> if c < 0.0 then - c else c
           showNegative = fun c v -> if c < 0.0 then "(" + v + ")" else v};
      NL, {dateFormat   = "dd-MM-yyyy";
           title        = "Datum      | Omschrijving              | Verandering  ";
           culture      = CultureInfo("nl-NL");
           neg          = id
           showNegative = fun _ v -> v}
    ] |> Map.ofList

let currencies = [USD,"$"; EUR,"€"] |> Map.ofList

type Entry = { dat: DateTime; des: string; chg: int }

let mkEntry date description change = { dat = DateTime.Parse(date, CultureInfo.InvariantCulture); des = description; chg = change }
        
let show currency locale entry = 
    let resx = resources.[getLocale locale]
    let newDes = 
        if entry.des.Length <= 25 
        then entry.des.PadRight(25) 
        else entry.des.[0..21] + "..."
    let c = float entry.chg / 100.0
    let ccc = getCurrency currency
    let isDefault = getLocale locale = US

    entry.dat.ToString(resx.dateFormat) + " | " + newDes + " | " 
        + (resx.showNegative c 
            (currencies.[ccc] 
              + (if isDefault then "" else " ") 
              + (resx.neg c).ToString("#,#0.00", resx.culture)) 
            + (if c < 0.0 then "" else " ")).PadLeft(13)

let formatLedger currency locale entries =
    let entryStr =
        entries
        |> List.sortBy (fun x -> x.dat, x.des, x.chg)
        |> List.map (show currency locale)
        |> List.fold (fun acc v -> acc + "\n" + v) ""
    resources.[getLocale locale].title + entryStr
