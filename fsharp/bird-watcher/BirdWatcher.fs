module BirdWatcher

let lastWeek: int [] = [| 0; 2; 5; 3; 7; 8; 4 |]

let yesterday (counts: int []) : int =
  let len = Array.length counts in counts.[len - 2]

let total (counts: int []) : int = Array.sum counts

let dayWithoutBirds (counts: int []) : bool = Array.exists ((=) 0) counts

let incrementTodaysCount (counts: int []) : int [] =
  counts.[counts.Length - 1] <- Array.last counts + 1
  counts

let oddWeek (counts: int []) : bool =
  let check days n = Array.forall (fun (_, x) -> x = n) days

  let evens, odds =
    Array.indexed counts
    |> Array.partition (fun (idx, _) -> idx % 2 = 0)

  check odds 0 || check odds 10 || check evens 5
