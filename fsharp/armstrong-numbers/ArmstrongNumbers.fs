module ArmstrongNumbers

let private digits n =
    let rec digits' xs x =
        if x < 10 then x :: xs
        else digits' ((x % 10) :: xs) (x / 10)
    digits' [] n

let isArmstrongNumber number =
    let xs = digits number
    List.sumBy (fun x -> pown x <| List.length xs) xs = number
