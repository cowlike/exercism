module BankAccount

type BankAccount = New | Closed | Account of float

let mkBankAccount () = New

// let openAccount acct = 
//     match acct with
//     | Account _ -> failwith "Hmmm..."
//     | _ -> Account 0.0

// let closeAccount acct =
//     match acct with
//     | Closed -> failwith "Account already closed"
//     | _ -> Closed

//screw it
let openAccount _ = Account 0.0
let closeAccount _ = Closed

let updateBalance n acct =
    match acct with
    | New -> Account n
    | Account m -> Account (m + n)
    | _ -> Closed

let getBalance acct =
    match acct with
    | Account n -> Some n
    | _ -> None
