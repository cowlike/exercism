module Accumulate

//non tail recursive
let rec accumulate' f list =
  match list with
  | [] -> []
  | x::xs -> f x :: accumulate' f xs

//tail recursive
let accumulate f list =
  let rec trAccum f acc list =
    match list with
    | [] -> acc
    | x::xs -> trAccum f (List.append acc [f x]) xs 
  trAccum f [] list
