module ETL

let transform input =
  let add k acc (s:string) = Map.add (s.ToLower()) k acc
  Map.fold (fun acc k v -> List.fold (add k) acc v) Map.empty input
