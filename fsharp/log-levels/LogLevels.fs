module LogLevels

let parseMsg (msg: string) =
  let components =
    msg.Replace('[', ' ').Replace(']', ' ').Split [|':'|]
    |> Array.map (fun s -> s.Trim())

  match components with
  | [|logType; logMsg|] -> logType.ToLower(), logMsg
  | _ -> "error", "oops"

let message = parseMsg >> snd

let logLevel = parseMsg >> fst

let reformat(logLine: string) = $"{message logLine} ({logLevel logLine})"
