module RobotSimulator

type Bearing = North | East | South | West

let movements = 
    [ Bearing.North, (0,0,1); 
      Bearing.East, (1,1,0); 
      Bearing.South, (2,0,-1); 
      Bearing.West, (3,-1,0) ] |> Map.ofList

let toEnum x = Map.pick (fun k (v,_,_) -> if v = x then Some k else None) movements

let fromEnum e = movements.[e] |> function (v,_,_) -> v
let mod' x y = (x % y + y) % y //because % is remainder and does not work as modulo for negative numbers in C/F#
let add x y = x + y
let flip f x y = f y x

let createRobot bearing (x, y) = (bearing, x, y)

let genericTurn fn bearing = 
    bearing
    |> fromEnum 
    |> fn 
    |> flip mod' 4 
    |> toEnum

let turnRight (bearing, x, y) = createRobot (genericTurn (add 1) bearing) (x, y)

let turnLeft (bearing, x, y) = createRobot (genericTurn (add (-1)) bearing) (x, y)

let advance (b, x, y) = let (_, x', y') = movements.[b] in createRobot b (x + x', y + y')

let takeAction robot =
    function
    | 'A' -> advance robot
    | 'L' -> turnLeft robot
    | 'R' -> turnRight robot
    | _   -> robot

let simulate robot actions = Seq.fold takeAction robot actions
