module ScrabbleScore

let flip f x y = f y x

let private letterScores =
  [ ("AEIOULNRST", 1);
    ("DG", 2);
    ("BCMP", 3);
    ("FHVWY", 4);
    ("K", 5);
    ("JX", 8);
    ("QZ", 10) ]
  |> Seq.collect (fun (k,v) -> Seq.map (fun c -> (c,v)) k)
  |> Map.ofSeq

let score (s:string) =
  s.Trim().ToUpper()
  |> Seq.map (flip Map.find letterScores)
  |> Seq.sum
