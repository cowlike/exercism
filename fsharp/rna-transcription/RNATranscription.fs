module RNATranscription

let convert c = 
    match c with
    | 'G' -> "C"
    | 'C' -> "G"
    | 'T' -> "A"
    | 'A' -> "U"
    | _ -> ""

let toRna dna = String.collect convert dna
