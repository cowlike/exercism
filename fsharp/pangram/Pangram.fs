module Pangram

open System

let private flip f x y = f y x

let isPangram (s:string) = List.forall (flip Seq.contains (s.ToLower())) ['a'..'z']
