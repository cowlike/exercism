﻿module Hamming

module Fold =
    ///distance using fold2
    let distance (s1: string) (s2: string): int option = 
        if String.length s1 <> String.length s2 then None
        else (s1, s2) 
            ||> Seq.fold2 (fun acc x y -> acc + (if x = y then 0 else 1)) 0
            |> Some

module TRPattern =
    ///distance using pattern matching, tail recursive
    let distance s1 s2 = 
        let rec calc lists sum = 
            match lists with
            | ([], []) -> Some sum
            | (x :: xs, y :: ys) -> calc (xs, ys) (sum + if x = y then 0 else 1)
            | _ -> None
        calc ((List.ofSeq s1), (List.ofSeq s2)) 0

module NotTR =
    ///direct operations on Seq, not tail recursive
    let rec distance s1 s2 = 
        if Seq.isEmpty s1 && Seq.isEmpty s2 then Some 0
        elif Seq.isEmpty s1 || Seq.isEmpty s2 then None
        else
            let x = if Seq.head s1 = Seq.head s2 then 0 else 1
            Option.map ((+) x) (distance (Seq.skip 1 s1) (Seq.skip 1 s2))

module NotTR2 =
    let rec distance (s1: 'a seq) (s2: 'a seq): int option = 
        if (Seq.isEmpty s1) && (Seq.isEmpty s2) then Some 0
        elif Seq.length s1 <> Seq.length s2 then None
        elif Seq.head s1 = Seq.head s2 then Option.map ((+)0) (distance (Seq.tail s1) (Seq.tail s2)) 
        else Option.map ((+)1) (distance (Seq.tail s1) (Seq.tail s2)) 

[<AutoOpen>]
module ZipSum =
    let distance s1 s2 =
        if Seq.length s1 <> Seq.length s2 then None
        else 
            Seq.zip s1 s2
            |> Seq.sumBy (fun (x,y) -> if x = y then 0 else 1)
            |> Some
