module PrimeFactors

let primeFactorsFor =
    let rec factors div lst n =
        if n < 2L then lst
        elif n % div = 0L then factors div (div :: lst) (n / div)
        else factors (div + 1L) lst n
    factors 2L [] >> List.rev
