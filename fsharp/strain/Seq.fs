module Seq

// let keep pred xs = seq {for x in xs do if pred x then yield x}

// let discard pred = keep (not << pred)

let partition pred =
    let part (t,f) v = if pred v then (Seq.append [v] t,f) else (t,Seq.append [v] f)
    Seq.fold part (Seq.empty, Seq.empty)

let keep f = Seq.rev << fst << partition f

let discard f = Seq.rev << snd << partition f
