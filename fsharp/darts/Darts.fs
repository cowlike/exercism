﻿module Darts

(*
let score (x: double) (y: double): int = 
    if x <= 0.0 || y <= 0.0 then failwith "Illegal argument"

    if x <= 1.0 && y <= 1.0 then 10
    elif x <= 5.0 && y <= 5.0 then 5
    elif x <= 10.0 && y <= 10.0 then 1
    else 0
*)

let ringScores = [
    (1.0, 10)
    (5.0, 5)
    (10.0, 1) ]

let inBounds x y (upper, _) = 
    x >= 0.0 && y >= 0.0 && x <= upper && y <= upper

let score x y =
    match List.tryFind (inBounds x y) ringScores with
    | Some (_,v) -> v
    | None       -> 0
