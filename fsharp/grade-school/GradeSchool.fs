module GradeSchool

let empty = Map.empty

let roster s =
  Map.toList s
  |> List.sort

let grade g s =
  match Map.tryFind g s with
  | Some x -> x
  | _ -> []

let add name g s =
  let newList = List.sort (name::(grade g s))
  Map.add g newList s
