module SimpleCipher

open System

let private join (xs:'a seq) = String.Join("", xs)

let private valid key =
    if key = "" || not <| Seq.forall (fun c -> Char.IsLower(c)) key 
    then invalidArg "Invalid key" key
    else key

let private shift f keyChar textChar =
    let intv c = int c - int 'a'
    let modulo x y = ((x % y) + y) % y

    modulo (f (intv textChar) (intv keyChar)) 26 + int 'a' |> char

let encode key = join << Seq.map2 (shift (+)) (valid key)

let decode key = join << Seq.map2 (shift (-)) (valid key)

let private randGen =
    let mutable n = 0
    fun () ->
        n <- n + 1
        Random(n)

let encodeRandom plainText =
    let generator = randGen()
    let key = List.init 100 (fun _ -> generator.Next(int 'a', int 'z') |> char) |> join
    (key, encode key plainText)
