module OcrNumbers

open System

type private Parts = S | B | L | R | BL | BR | LR | LRB | Malformed

let private part s = 
    match List.ofSeq s with
        | [' ';' ';' '] -> S
        | [' ';'_';' '] -> B
        | ['|';' ';' '] -> L
        | [' ';' ';'|'] -> R
        | ['|';'_';' '] -> BL
        | [' ';'_';'|'] -> BR
        | ['|';' ';'|'] -> LR
        | ['|';'_';'|'] -> LRB
        | _ -> Malformed

let private number lst = 
    match (List.map part lst) with
    | [B; LR; LRB; S] -> "0"
    | [S; R; R; S] -> "1"
    | [B; BR; BL; S] -> "2"
    | [B; BR; BR; S] -> "3"
    | [S; LRB; R; S] -> "4"
    | [B; BL; BR; S] -> "5"
    | [B; BL; LRB; S] -> "6"
    | [B; R; R; S] -> "7"
    | [B; LRB; LRB; S] -> "8"
    | [B; LRB; BR; S] -> "9"
    | _ -> "?"

let private isValidShape xs =
    List.length xs % 4 = 0 && List.forall (fun sublist -> Seq.length sublist % 3 = 0) xs

let private lines input = 
    input
    |> List.ofSeq
    |> List.chunkBySize 4
    |> function
        | xs when List.forall isValidShape xs -> Some xs
        | _ -> None

let rec private transpose = function
    | (_::_)::_ as m -> List.map List.head m :: transpose (List.map List.tail m)
    | _ -> []

let private tokenizeLine lst =
    let tokenize = List.ofSeq >> List.chunkBySize 3
    List.map tokenize lst |> transpose

let asNumber input =
    input 
    |> List.map (List.map number)
    |> List.map (List.fold (+) "")
    |> (fun lst -> String.Join(",", lst))

let convert input =
    lines input 
    |> Option.map (List.map tokenizeLine >> asNumber)
