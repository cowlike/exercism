module SimpleLinkedList

type LinkedList<'a> = Nil | Node of 'a * LinkedList<'a>

let nil = Nil

let create v list = Node (v, list)

let isNil list = list = Nil

let fromList list = List.foldBack create list Nil

let rec toList = function
    | Nil -> []
    | Node (v, lst) -> v :: toList lst

let toListTR list = //for Eric!
    let rec toLst acc = function
        | Nil -> acc
        | Node (v, lst) -> toLst (acc @ [v]) lst
    toLst [] list

let next = function
    | Nil -> Nil
    | Node (_, lst) -> lst

let datum = function
    | Nil -> failwith "empty list"
    | Node (v, _) -> v

let rec reverse list = 
    let rec rList acc = function
        | Nil -> acc
        | Node (v, lst) -> rList (create v acc) lst
    rList Nil list
