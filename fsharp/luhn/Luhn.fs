module Luhn

open System

let private flip f x y = f y x

let private d x = let x1 = x * 2 in if x1 > 9 then x1 - 9 else x1

let rec private doubleEveryOther acc = 
    function
    | [] -> acc
    | [x] -> x :: acc
    | (x :: y :: xs) -> doubleEveryOther (d y :: x :: acc) xs

let private rsum xs =
    Seq.rev xs
    |> List.ofSeq
    |> List.map (int >> flip (-) (int '0'))
    |> doubleEveryOther []
    |> List.sum 

let valid cardNum =
    let digits = Seq.filter (not << Char.IsWhiteSpace) cardNum
    Seq.forall Char.IsDigit digits 
    && Seq.length digits > 1 
    && rsum digits % 10 = 0
