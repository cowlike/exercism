module LuhnTest

open NUnit.Framework
open Luhn

[<Test>]
let ``Single digit strings cannot be valid`` () =
    let input = "1"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``A single zero is invalid`` () =
    let input = "0"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Single zero with space is invalid`` () =
    let input = "0 "
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Lots of zeros are valid`` () =
    let input = "00000"
    let expected = true
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Nine doubled is nine`` () =
    let input = "091"
    let expected = true
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Simple valid number`` () =
    let input = " 5 9 "
    let expected = true
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Valid Canadian SIN`` () =
    let input = "046 454 286"
    let expected = true
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Another valid SIN`` () =
    let input = "055 444 285"
    let expected = true
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Invalid Canadian SIN`` () =
    let input = "046 454 287"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Invalid credit card`` () =
    let input = "8273 1232 7352 0569"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Strings that contain non-digits are not valid`` () =
    let input = "827a 1232 7352 0569"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Punctuation is not allowed`` () =
    let input = "055-444-285"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))

[<Test>]

let ``Symbols are not allowed`` () =
    let input = "055£ 444$ 285"
    let expected = false
    let actual = valid input
    Assert.That(actual, Is.EqualTo(expected))