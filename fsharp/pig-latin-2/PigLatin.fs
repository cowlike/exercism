﻿module PigLatin

open System.Text.RegularExpressions

let vowels = "aeiou"
let quPattern = @"(^qu|^.qu)(.*)"
let initialVowelPattern = sprintf @"(^[%s]|^y[^%s]).*" vowels vowels
let nextVowelPattern = sprintf @"(.[^y%s]*)(.*)" vowels

let matches s pattern = Regex.Match(s, pattern).Success

let matchGroups s pattern =
    Regex.Match(s, pattern).Groups 
    |> Seq.cast<Group> 
    |> Seq.map (fun m -> m.Value)
    |> Array.ofSeq

let translateWord = function
    | s when matches s initialVowelPattern -> s + "ay"
    | s when matches s quPattern ->
        let groups = matchGroups s quPattern
        groups.[2] + groups.[1] + "ay"
    | s -> 
        let groups = matchGroups s nextVowelPattern
        let suffix = groups.[2]
        if suffix = "ay" then groups.[0] + "ay" 
        else suffix + groups.[1] + "ay"

let translate (s: string) = 
    System.String.Join(" ", s.Split [|' '|] |> Array.map translateWord)
