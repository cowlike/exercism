module ErrorHandling

open System

type Result<'a,'b> = Ok of 'a | Error of 'b

let handleErrorByThrowingException() = failwith "oops"

let handleErrorByReturningOption s =
  let (good, n) = Int32.TryParse(s)
  if good then Some n else None

let handleErrorByReturningResult s =
  let (good, n) = Int32.TryParse(s)
  if good then Ok n else Error "Could not convert input to integer"

let cleanupDisposablesWhenThrowingException x =
  using x (fun _ -> failwith "oops")

//bind :: (fun a -> mb) -> ma -> mb
let bind f =
  function
  | Ok n -> f n
  | e -> e
