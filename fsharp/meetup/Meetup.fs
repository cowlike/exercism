module Meetup

open System

type Schedule = First | Second | Third | Fourth | Last | Teenth

let private addDays n (day:DateTime) = day.AddDays(n)

let private pred = addDays -1.0

let private succ = addDays 1.0

let rec private findByDay f weekday (day:DateTime) = 
    match day with
    | day when day.DayOfWeek = weekday -> day
    | _ -> findByDay f weekday (f day)

let private nthDay n year month weekday = 
    new DateTime(year, month, 1) 
    |> findByDay succ weekday 
    |> addDays (7.0 * (float n))

let rec meetupDay weekday sched year month =
    match sched with
    | First -> nthDay 0 year month weekday
    | Second -> nthDay 1 year month weekday
    | Third ->nthDay 2 year month weekday
    | Fourth -> nthDay 3 year month weekday
    | Last -> (new DateTime(year, month, 1)).AddMonths(1) |> pred |> findByDay pred weekday
    | Teenth -> new DateTime(year, month, 13) |> findByDay succ weekday
