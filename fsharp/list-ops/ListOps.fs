module ListOps

let rec foldl f acc = function
    | [] -> acc
    | x::xs -> foldl f (f acc x) xs

let flip f x y = f y x

let reverse xs = foldl (fun acc v -> v :: acc) [] xs

let foldr f xs acc = foldl (flip f) acc (reverse xs)

let length xs = foldl (fun acc _ -> acc + 1) 0 xs

let map f xs = foldr (fun v acc -> (f v) :: acc) xs []

let filter f xs = [ for x in xs do if f x then yield x ]

let append xs ys = foldr (fun v acc -> printfn "%A %A" v acc; v :: acc) xs ys

let concat xs = foldr append xs []

let xs = List.map (fun x -> [x]) [1..10]
foldl append [] xs