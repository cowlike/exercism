#r "FakeLib.dll"
open Fake 

let getFSFiles = 
    [ for file in (filesInDirMatching "*.fs" (directoryInfo currentDirectory)) -> file.Name ]

let getTestFile =
    getFSFiles |> List.filter (fun file -> file.EndsWith "Test.fs")

let getNonTestFiles =
    getFSFiles |> List.filter (fun file -> not(file.EndsWith "Test.fs"))

let getBuildOrder =
    getNonTestFiles @ getTestFile

//make functions
let build = fun () ->
    trace " --- Building --- "
    let order = getBuildOrder
    tracefn "The build order will be: %A" order
    order |> FscHelper.compile [
            FscHelper.Target FscHelper.TargetType.Library
            FscHelper.References [currentDirectory + "\\..\\nunit\\nunit.framework.dll"]
    ] |> function 0 -> () | c -> failwithf "exited with status %d" c
    

let test = fun() -> 
    trace "--- Testing ---"
    !! (currentDirectory + @"\*.dll") 
      |> NUnit (fun p -> { p with 
        ErrorLevel = DontFailBuild;
        ToolPath = currentDirectory + "\\..\\nunit\\";
    })
//Targets
Target "Test" (test)

Target "build" (build)

//deps go here!
"build"
    ==> "test"

RunTargetOrDefault "test"