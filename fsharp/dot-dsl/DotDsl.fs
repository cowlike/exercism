module DotDsl

type Pair<'a> = 'a * 'a

type GraphElement = 
    Attr of Pair<string>
    | Node of string * list<Pair<string>> 
    | Edge of string * string * list<Pair<string>>

type Graph = Graph of GraphElement list

let attr name value = Attr (name, value)

let edge label1 label2 attrs = Edge (label1, label2, attrs)

let node label attrs = Node (label, attrs)

let graph elems = Graph elems

let filteredView (Graph elements) f = List.filter f elements |> List.sort

let nodes g = filteredView g (function Node(_,_) -> true | _ -> false)

let attrs g = filteredView g (function Attr(_,_) -> true | _ -> false)

let edges g = filteredView g (function Edge(_,_,_) -> true | _ -> false)
