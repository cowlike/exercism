﻿module ComplexNumbers

open System

type ComplexNumber = { real: double; imaginary: double }

let t4 c1 c2 =
  (c1.real, c1.imaginary, c2.real, c2.imaginary)

let create real imaginary = { real = real; imaginary = imaginary }

let mul z1 z2 =
  let (a, b, c, d) = t4 z1 z2 in

  { real = a * c - b * d
    imaginary = b * c + a * d }

let add z1 z2 =
  let (a, b, c, d) = t4 z1 z2 in { real = a + c; imaginary = b + d }

let sub z1 z2 =
  let (a, b, c, d) = t4 z1 z2 in { real = a - c; imaginary = b - d }

let div z1 z2 =
  let (a, b, c, d) = t4 z1 z2 in

  { real = (a * c + b * d) / (c * c + d * d)
    imaginary = (b * c - a * d) / (c * c + d * d) }

let abs { real = r; imaginary = i } = sqrt (r * r + i * i)

let conjugate { real = r; imaginary = i } = { real = r; imaginary = -i }

let real { real = r } = r

let imaginary { imaginary = i } = i

let rec exp { real = r; imaginary = i } =
  if r = 0.0 then
    { real = cos i; imaginary = sin i }
  else
    exp { real = 0.0; imaginary = i }
    |> mul { real = Math.E ** r; imaginary = 0.0 }
