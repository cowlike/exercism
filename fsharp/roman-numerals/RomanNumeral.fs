module RomanNumeral

let private convert decimalPlace n =
    let digits = [|("I","V"); ("X","L"); ("C","D"); ("M","?"); ("?","?")|]
    
    if decimalPlace < 0 || decimalPlace > 3 then []
    else
        let (d0,d1) = digits.[decimalPlace]
        let (d2,_) = digits.[decimalPlace + 1] 
        match n with
            | n when n < 4 -> List.replicate n d0
            | 4 -> [d0; d1]
            | 5 -> [d1]
            | n when n < 9 -> d1 :: List.replicate (n - 5) d0
            | 9 -> [d0; d2]
            | _ -> []

let toRoman n =
    let rec decimal lst = function
        | 0 -> lst
        | n -> decimal (lst @ [n % 10]) (n / 10)
    
    decimal [] n
    |> List.mapi convert
    |> List.fold (fun acc v -> v @ acc) []
    |> List.fold (+) ""
