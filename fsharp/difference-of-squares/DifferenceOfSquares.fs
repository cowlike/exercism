module DifferenceOfSquares

let squareOfSums x =
    let s = List.sum [1..x] in s*s

let sumOfSquares x =
    [1..x] |> List.sumBy (fun x -> x*x) 

let difference x = squareOfSums x - sumOfSquares x
