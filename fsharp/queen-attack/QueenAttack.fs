module QueenAttack

let canAttack ((x1,y1) as p1) ((x2,y2) as p2) =
    if (p1 = p2) 
        then failwith "Cannot occupy same square"
    else 
        x1 = x2 || y1 = y2 || (abs (x1 - x2) = abs (y1 - y2))
