module Isogram

open System

let isogram str = 
    let chars = str |> String.map Char.ToUpper |> Seq.filter Char.IsLetter
    Seq.length chars = (Seq.length <| Seq.distinct chars)