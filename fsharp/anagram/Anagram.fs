module Anagram

let tApply f (e1, e2) = f e1, f e2

let lower (s:string) = s.ToLower()

let normalize = lower >> Seq.sort >> List.ofSeq

let isAnagram w1 w2 = 
    ((w1,w2) |> (tApply lower) ||> (<>)) && 
    ((w1,w2) |> (tApply normalize) ||> (=))   

let anagrams words input = List.filter (isAnagram input)  words
