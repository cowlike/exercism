module NucleoTideCount

let nucleotides = "ACTG"
let init =
  Seq.initInfinite (fun _ -> 0)
  |> Seq.zip nucleotides
  |> Map.ofSeq

let valid c =
  match Seq.contains c nucleotides with
  | true -> c
  | _ -> failwith "Invalid nucleotide"

let update f (map:Map<char,int>) k = Map.add (valid k) (f map.[k]) map

let nucleotideCounts strand = Seq.fold (update ((+) 1)) init strand

let count c strand = (nucleotideCounts strand).[valid c]
