﻿module DiffieHellman

open System.Numerics
open System.Security.Cryptography

/// get a random bigint from 2..(primeP-1), inclusive
let privateKey (p: bigint) = 
    let nBytes = p.ToByteArray() |> Array.length
    let bArray = Array.create nBytes 0uy
    let rng = new RNGCryptoServiceProvider()
    rng.GetBytes bArray
    let bi = BigInteger (Array.append bArray [|0uy|])
    let range = p - 2I
    bi % range + 2I

let publicKey p g privateKey =
    BigInteger.ModPow(g, privateKey, p)

let secret p publicKey privateKey =
    BigInteger.ModPow(publicKey, privateKey, p)
