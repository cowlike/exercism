module Grep

open System.IO
open System

type Flag = WholeLine | Invert | CaseInsensitive | PathOnly | LineNo

let private flip f x y = f y x

let private (?=) b (x,y) = if b then x else y

let private parse (flags: string) =
    let allFlags = [
        ("x", WholeLine); 
        ("v", Invert); 
        ("i", CaseInsensitive); 
        ("l", PathOnly); 
        ("n", LineNo)] |> Map.ofList
    let folder acc v = 
        match (Map.tryFind (string v) allFlags) with 
        | Some t -> t :: acc 
        | _ -> acc
    Seq.fold folder [] flags

let private mkCompare flag = 
    let contains (input: string) pattern = input.Contains(pattern)
    let c = flag WholeLine ?= ((=), contains)
    let comp = flag Invert ?= ((fun x -> c x >> not), c)
    let i = flag CaseInsensitive ?= ((fun (x:string) -> x.ToLower()), id)

    fun x y -> comp (i x) (i y)

let private parseFlags flags numFiles = 
    let flag = flip List.contains (parse flags)
    let ofBool cond v = if cond then Some v else None
    let addPath = (fun p -> numFiles > 1 ?= (p+":", ""))
    let cf = mkCompare flag

    match List.tryFind flag [PathOnly; LineNo] with
    | Some PathOnly ->
        let mutable once = ""
        (fun pattern path _ text ->
            let b = cf text pattern
            if not b || (once = path && b) then None
            else once <- path; Some path)
    | Some LineNo ->
        (fun pattern path line text -> 
          ofBool (cf text pattern) (sprintf "%s%d:%s" (addPath path) line text))
    | _ -> (fun pattern path _ text -> ofBool (cf text pattern) (sprintf "%s%s" (addPath path) text))

let grep pattern flags files = 
    let searchFunc = parseFlags flags (List.length files)

    let show pattern path =
        File.ReadLines(path)
        |> Seq.mapi (fun n line -> searchFunc pattern path (n+1) line)
        |> Seq.choose id
        |> Seq.fold (fun acc v -> acc + v + "\n") ""

    List.map (show pattern) files |> String.concat ""
