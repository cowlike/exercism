﻿module Markdown

open System.Text.RegularExpressions

let matches pattern s = Regex.Match(s, pattern).Success

let matchGroups pattern s =
    Regex.Match(s, pattern).Groups 
    |> Seq.cast<Group> 
    |> Seq.map (fun m -> m.Value)
    |> List.ofSeq

let header s =
    let pattern = @"(#+) (.*)"
    match matchGroups pattern s with
    | [_; g1; g2] -> let hx = String.length g1
                     sprintf "<h%d>%s</h%d>" hx g2 hx
    | _ -> s                  

let wrap line =
    let replace pattern text (s:string) =
        match matchGroups pattern s with
        | [_; g1; g2; g3] -> sprintf "%s<%s>%s</%s>%s" g1 text g2 text g3
        | _ -> s                  
    line
    |> replace @"^(.*)__(.*)__(.*)" "strong"
    |> replace @"^(.*)_(.*)_(.*)" "em"

let parse (markdown: string) =
    let li s = "<li>" + s + "</li>"
    let (inList, html) =
        ((false, ""), markdown.Split('\n'))
        ||> Array.fold(fun (inList, acc) line ->
            match Seq.head line with
            | '*' -> let ul = if inList then "" else "<ul>"
                     true, acc + ul + (wrap (line.Substring 2) |> li)
            | '#' -> inList, header line
            | _   -> inList, acc + "<p>" + wrap line + "</p>")

    if inList then html + "</ul>" else html
