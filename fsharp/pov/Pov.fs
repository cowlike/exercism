﻿module Pov

type 'a Graph = { value: 'a; children: 'a Graph list }

let mkGraph<'a> value children : 'a Graph = { value = value; children = children }

let pathToChild value (parent: 'a Graph) =
  let rec loop value parent acc =
    if parent.value = value then
      (parent.value :: acc)
    else
      List.collect (fun g -> loop value g (parent.value :: acc)) parent.children

  loop value parent [] |> List.rev

let promoteChild value (parent: 'a Graph) =
  if value = parent.value then
    Some parent
  else
    let withChild, withoutChild =
      List.partition (fun g -> g.value = value) parent.children

    match withChild with
    | [] -> None
    | x :: _ ->
      mkGraph x.value (x.children @ [ mkGraph parent.value withoutChild ])
      |> Some

let fromPOV value tree =
  match pathToChild value tree with
  | [] -> None
  | path -> List.fold (fun acc v -> Option.bind (promoteChild v) acc) (Some tree) path

let tracePathBetween a b tree =
  match fromPOV a tree |> Option.map (pathToChild b) with
  | Some [] -> None
  | path -> path
