module BracketPush

let rec private paired matches acc = function
    | [] -> List.isEmpty acc
    | x :: xs -> 
        match (List.tryFind (fun (l,r) -> x = l || x = r) matches) with
        | None -> paired matches acc xs
        | Some (l,r) -> 
            if x = r then
                if List.isEmpty acc || List.head acc <> l then false
                else paired matches (List.tail acc) xs
            else paired matches (x :: acc) xs

let matched input = 
    let matches = [('{','}'); ('[',']'); ('(', ')')]
    paired matches [] (List.ofSeq input)
