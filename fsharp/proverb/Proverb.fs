module Proverb

let private verses =
  let flip f x y = f y x
  let verse [|a;b|] = sprintf "For want of a %s the %s was lost." a b
  ["nail";"shoe";"horse";"rider";"message";"battle";"kingdom"]
  |> Seq.windowed 2
  |> Seq.map verse
  |> flip Seq.append ["And all for the want of a horseshoe nail."]
  |> List.ofSeq

let line n = verses.[n-1]

let proverb = System.String.Join("\n", List.map line [1..7])
