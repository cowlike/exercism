module SaddlePoints

let rec transpose = function
    | (_::_)::_ as m -> List.map List.head m :: transpose (List.map List.tail m)
    | _ -> []

let saddlePoints values =
    let transVal = transpose values
    let rows = (List.length (List.head values)) - 1
    let cols = (List.length values) - 1
    [ for y in [0 .. rows] do
      for x in [0 .. cols] do
        let v = values.[x].[y]
        if (v = List.max values.[x] && v = List.min transVal.[y]) then yield (x,y)]
