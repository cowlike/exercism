﻿module IsbnVerifier

open System.Text.RegularExpressions

let isValid (isbn: string) =
    let getValue c = if c = 'X' then 10 else int c - int '0'
    let digits = isbn.Replace("-", "")

    Regex.Match(digits, @"^\d{9}[\dX]$").Success &&
    digits
    |> Seq.map2 (fun mult c -> mult * getValue c) [10..-1..1]
    |> Seq.sum
    |> fun x -> x % 11 = 0
