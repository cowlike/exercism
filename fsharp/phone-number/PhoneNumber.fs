module PhoneNumber

open System

let parsePhoneNumber str = 
    let pn = Seq.filter Char.IsDigit str |> Seq.truncate 12
    let mkStr (s:seq<char>) = String.Join("", s)

    match (Seq.length pn) with
    | 10 -> (mkStr pn) |> Some
    | 11 when Seq.head pn = '1' -> Seq.tail pn |> mkStr |> Some
    | _ -> None


