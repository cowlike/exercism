module LinkedList

type 'a Node = { 
    value : 'a; 
    mutable prev : 'a Node option; 
    mutable next : 'a Node option }

type 'a LinkedList = { 
    front : 'a Node option; 
    back : 'a Node option }

let mkLinkedList = { front = None; back = None }

let (>>=) m f = Option.bind f m

let peek node = Some node.value

let prev node = node.prev

let next node = node.next

let setPrev n node =
    node.prev <- n
    Some node

let setNext n node = 
    node.next <- n
    Some node

//push and pop operate on back

let push v list =
    let b = list.back
    let n = Some {value=v; prev=b; next=None}
    b >>= setNext n |> ignore
    { front = (if Option.isSome list.front then list.front else n); back = n }

let pop list =
    let b = list.back
    let b' = b >>= prev >>= setNext None
    ((b >>= peek) |> Option.get, 
        { front = (if Option.isSome b' then list.front else b'); back = b' }) 

//unshift and shift operate on front

let unshift v list =
    let f = list.front
    let n = Some {value=v; prev=None; next=f}
    f >>= setPrev n |> ignore
    { front = n; back = if Option.isSome list.back then list.back else n; }

let shift list = 
    let f = list.front
    let f' = f >>= next >>= setPrev None
    ((f >>= peek) |> Option.get, 
        { front = f'; back = if Option.isSome f' then list.back else f' }) 
