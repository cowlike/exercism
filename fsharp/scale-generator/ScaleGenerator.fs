module ScaleGenerator

let private distance = function
    | 'm' -> 1
    | 'M' -> 2
    | 'A' -> 3
    | _ -> 0

let private capitalize (s:string) = (System.Char.ToUpper(s.[0]) |> string) + (s.[1..]);

let private butlast list = Seq.rev list |> Seq.tail |> Seq.rev

let private pitchList tonic = 
    let chromaticSharps = [|"A"; "A#"; "B"; "C"; "C#"; "D"; "D#"; "E"; "F"; "F#"; "G"; "G#"|]
    let chromaticFlats = [|"A"; "Bb"; "B"; "C"; "Db"; "D"; "Eb"; "E"; "F"; "Gb"; "G"; "Ab"|]
    let useSharps = ["a"; "C"; "G"; "D"; "A"; "E"; "B"; "F#"; "e"; "b"; "f#"; "c#"; "g#"; "d#"]
    let useFlats = ["F"; "Bb"; "Eb"; "Ab"; "Db"; "Gb"; "d"; "g"; "c"; "f"; "bb"; "eb"]
    if List.contains tonic useSharps then chromaticSharps else chromaticFlats

let pitches tonic steps =
    let pitches' = pitchList tonic
    let start = Array.findIndex ((=) (capitalize tonic)) pitches'
    let stepDistances = 
        Seq.fold (fun acc v -> distance v + List.head acc :: acc) [start] (butlast steps)
    
    Seq.fold (fun acc n -> pitches'.[(n % (Array.length pitches'))] :: acc) [] stepDistances
