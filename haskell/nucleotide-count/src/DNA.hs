module DNA (count, nucleotideCounts) where

import qualified Data.Map as M
import Control.Monad

nucleotides = "AGCT"

count :: Char -> String -> Either String Int
count c s = length <$> filterM (same c) s

same :: Char -> Char -> Either String Bool
same a b = (==) <$> valid a <*> valid b

valid :: Char -> Either String Char
valid c | c `elem` nucleotides = Right c
        | otherwise = Left ("invalid nucleotide " ++ show c)

nucleotideCounts :: String -> Either String (M.Map Char Int)
nucleotideCounts s = foldM
  (\acc c -> count c s >>= (\v -> return $ M.adjust (+v) c acc))
  (M.fromList $ zip nucleotides (repeat 0))
  nucleotides
