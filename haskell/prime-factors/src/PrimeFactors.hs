module PrimeFactors (primeFactors) where

primeFactors :: Integer -> [Integer]
primeFactors num = reverse $ factors 2 [] num where
    factors d lst n
        | n < 2 = lst
        | n `mod` d == 0 = factors d (d : lst) (n `div` d)
        | otherwise = factors (d + 1) lst n
