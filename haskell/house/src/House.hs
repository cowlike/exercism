module House (rhyme) where

import Data.List (init)

phrases :: [(String, String)]
phrases = [
    ("house that Jack built.", ""),
    ("malt", "lay in"),
    ("rat", "ate"),
    ("cat", "killed"),
    ("dog", "worried"),
    ("cow with the crumpled horn", "tossed"),
    ("maiden all forlorn", "milked"),
    ("man all tattered and torn", "kissed"),
    ("priest all shaven and shorn", "married"),
    ("rooster that crowed in the morn", "woke"),
    ("farmer sowing his corn", "kept"),
    ("horse and the hound and the horn", "belonged to")
    ]

buildVerse :: (String, String) -> String -> String
buildVerse (s1,s2) v = v ++ " the " ++ s1 ++ line s2 where
        line "" = ""
        line s = "\nthat " ++ s

verse :: [(String, String)] -> Int -> String
verse ps n
        | n > 0 && n < 13 = foldr buildVerse "This is" (take n ps)
        | otherwise =  ""

rhyme :: String
rhyme = init $ foldl (\acc v -> acc ++ v ++ "\n\n") "" $ map (verse phrases) [1..12]
