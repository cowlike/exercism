module Raindrops (convert) where

import Data.Maybe
import Control.Applicative

convert' :: Int -> String
convert' num 
    | null result = show num
    | otherwise = result
    where 
        divisors = [(3,"Pling"), (5,"Plang"), (7,"Plong")]
        result = concatMap (\(n,s) -> if num `mod` n == 0 then s else "") divisors


check :: (String, Int) -> Int -> Maybe String
check (c, divisor) n 
    | n `mod` divisor == 0 = Just ("Pl" ++ c ++ "ng") 
    | otherwise = Nothing

convert :: Int -> String
convert n = 
    fromJust $ 
    foldl1 mappend (check <$> [("i",3), ("a",5), ("o",7)] <*> pure n) 
    <|> (Just $ show n)
