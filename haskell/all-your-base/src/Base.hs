module Base (rebase) where

import Data.List (foldl')
import Control.Monad (guard)

toVal :: Integral a => a -> [a] -> a
toVal numBase = foldl' (\acc v -> acc * numBase + v) 0

fromVal :: Integral a => a -> a -> [a]
fromVal numBase num = if num == 0 then [] else convert [] numBase num where
    convert acc _ 0 = acc
    convert acc b n = convert (n `mod` b : acc) b (n `div` b)

rebase :: Integral a => a -> a -> [a] -> Maybe [a]
rebase inputBase outputBase inputDigits = do
  guard $ all (>= 2) [inputBase, outputBase]
  guard $ all (\x -> x < inputBase && x >= 0) inputDigits
  let n = toVal inputBase inputDigits
  return $ fromVal outputBase n
