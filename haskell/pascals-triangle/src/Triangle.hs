module Triangle (rows) where

grow :: Num a => [a] -> [a] -> [a]
grow acc [] = acc
grow acc [x] = x : acc
grow acc (x:y:xs) = grow (x+y : acc) $ y:xs

rows :: Int -> [[Integer]]
rows n
    | n <= 0 = []
    | otherwise = take n $ iterate (grow [1]) [1]
