module Triplet (isPythagorean, mkTriplet, pythagoreanTriplets) where

import Data.List (sort)

isPythagorean :: (Num a, Eq a) => (a, a, a) -> Bool
isPythagorean (a,b,c) = a*a + b*b == c*c

mkTriplet :: Ord t => t -> t -> t -> (t, t, t)
mkTriplet a b c = let [x,y,z] = sort [a,b,c] in (x,y,z)

pythagoreanTriplets :: (Num a, Ord a, Enum a) => a -> a -> [(a, a, a)]
pythagoreanTriplets from to =
    [mkTriplet a b c | a <- [from..to], b <- [a..to], c <- [b..to], isPythagorean (a,b,c)]
