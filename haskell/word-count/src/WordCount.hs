module WordCount (wordCount, mySplit) where

import Data.Char
import Data.List.Split
import Data.List
import qualified Data.Map as M

wordCount :: String -> M.Map String Int
wordCount = foldr (\ v acc -> M.insertWith (+) v 1 acc) M.empty . mySplit

mySplit :: String -> [String]
mySplit = words . map letters where
  letters c
    | isAlphaNum c = toLower c
    | otherwise = ' '
