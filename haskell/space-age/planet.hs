module SpaceAge (
  Planet(..)
  , ageOn
) where

import qualified Data.Map as Map
import Data.Maybe(fromJust)

data Planet = Mercury | Venus | Earth | Mars | Jupiter | Saturn | Uranus | Neptune deriving (Eq, Ord)

factors :: Map.Map Planet Double
factors = Map.fromList [
    (Mercury, 0.2408467)
    , (Venus, 0.61519726)
    , (Earth, 1.0)
    , (Mars, 1.8808158)
    , (Jupiter, 11.862615)
    , (Saturn, 29.447498)
    , (Uranus, 84.016846)
    , (Neptune, 164.79132)
  ]

earthSecPerYear :: Integer
earthSecPerYear = 31557600

ageOn :: Planet -> Integer -> Double
ageOn p sec = fromInteger sec
  / fromInteger earthSecPerYear
  / fromJust (Map.lookup p factors)
