module Squares (difference, squareOfSums, sumOfSquares) where

difference :: Integral a => a -> a
difference x = squareOfSums x - sumOfSquares x

squareOfSums :: Integral a => a -> a
squareOfSums x = let s = sum [1..x] in (s*s)

sumOfSquares :: Integral a => a -> a
sumOfSquares x = sum $ map (\y -> y*y) [1..x]
