module Matrix (saddlePoints) where

import GHC.Arr (Ix)
import Data.Array (Array, assocs)

saddlePoints :: (Ix t, Ord b) => Array (t, t) b -> [(t, t)]
saddlePoints m = map fst $ filter (saddlePoint m) $ assocs m

saddlePoint :: (Ix t, Ord a) => Array (t, t) a -> ((t, t), a) -> Bool
saddlePoint matrix ((r,c), v) = v >= maxRow && v <= minCol where
    (_, maxRow) = minMax $ row r matrix
    (minCol, _) = minMax $ col c matrix
    minMax xs = let vs = map snd xs in (minimum vs, maximum vs)

row :: Ix a => a -> Array (a, a) t -> [((a, a), t)]
row n arr = filter ((== n) . fst . fst) $ assocs arr

col :: Ix a => a -> Array (a, a) t -> [((a, a), t)]
col n arr = filter ((== n) . snd . fst) $ assocs arr