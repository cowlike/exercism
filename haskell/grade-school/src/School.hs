module School (School, add, empty, grade, sorted) where

import qualified Data.List as L
import qualified Data.Map as M

-- The task is to create the data type `School` and
-- implement the functions below.
newtype School = School (M.Map Int [String])

add :: Int -> String -> School -> School
add grade name (School m) = School $
  snd $ M.insertLookupWithKey (\k new old -> new ++ old) grade [name] m

empty :: School
empty = School $ M.fromList []

grade :: Int -> School -> [String]
grade g (School m) = M.findWithDefault [] g m

sorted :: School -> [(Int, [String])]
sorted (School m) = map sortNames (L.sort $ M.toList m) where
  sortNames (k,v) = (k, L.sort v)
