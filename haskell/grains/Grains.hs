module Grains (square, total) where

square :: Int -> Maybe Integer
square x
  | x < 1 || x > 64 = Nothing
  | otherwise = Just $ product $ replicate (x - 1) 2

total :: Integer
total = sum $ (sum . square) <$> [1..64]
