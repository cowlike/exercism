module Series (slices) where

import Data.Char (isDigit, digitToInt)

slices :: Int -> String -> [[Int]]
slices n xs
    | n == 0 = [[]]
    | null xs || n > length xs || (not . all isDigit) xs = []
    | otherwise = windowed n $ map digitToInt xs

windowed :: Int -> [Int] -> [[Int]]
windowed n xs | n >= length xs = [xs]
windowed n xs = take n xs : windowed n (tail xs)