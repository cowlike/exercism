module Allergies (Allergen(..), allergies, isAllergicTo) where

import Data.Bits
-- import Control.Monad.State

data Allergen = 
    Eggs 
    | Peanuts 
    | Shellfish 
    | Strawberries 
    | Tomatoes 
    | Chocolate 
    | Pollen 
    | Cats deriving (Eq, Enum, Show)

allergies :: Int -> [Allergen]
allergies n = filter (`isAllergicTo` n) $ enumFrom Eggs

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo allergen score = testBit score (fromEnum allergen)

type Stack = [Int]  
  
pop :: Stack -> (Int,Stack)  
pop (x:xs) = (x,xs)
pop [] = error "ohno!"  
  
push :: Int -> Stack -> ((),Stack)  
push a xs = ((),a:xs)

stackManip :: Stack -> (Int, Stack)  
stackManip stack = let  
    ((),newStack1) = push 3 stack  
    (a ,newStack2) = pop newStack1  
    in pop newStack2  

folder :: (Int, [Allergen]) -> Allergen -> (Int, [Allergen])
folder y@(x, acc) allergy =
    if isAllergicToV allergy x 
    then (x - 2 ^ fromEnum allergy, acc ++ [allergy])
    else y

allergiesV :: Int -> [Allergen]
allergiesV input
  | input >= 256 = allergies $ input - 256
  | otherwise = snd $ foldl folder (input, []) [Cats, Choca ..]

isAllergicToV :: Allergen -> Int -> Bool
isAllergicToV allergy num = num - 2 ^ fromEnum allergy >= 0

newtype State s a = State { runState :: s -> (a,s) }

instance Monad (State s) where  
    return x = State $ \s -> (x,s)  
    (State h) >>= f = State $ \s -> let (a, newState) = h s  
                                        (State g) = f a  
                                    in  g newState 

-- foo = 
--     let (a:b:xs) = [10,20,30]
--         d = a+b
--     in  show d