module Hamming (distance) where

distance :: String -> String -> Maybe Int
distance s1 s2
  | length s1 /= length s2 = Nothing
  | otherwise = Just . sum $ zipWith (\x y -> if x == y then 0 else 1) s1 s2
