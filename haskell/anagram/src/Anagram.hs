module Anagram where

import Data.List(sort)
import Data.Char(toLower)

anagramsFor :: String -> [String] -> [String]
anagramsFor x = filter (\s -> lower x /= lower s && sLower x == sLower s) where
  lower = map toLower
  sLower = sort . lower
