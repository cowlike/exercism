module DNA (toRNA) where

-- | if string contains invalid character, return Nothing
-- | if string contains only valid nucleotides, return Just transcription
toRNA :: String -> Maybe String
toRNA = mapM trans where
  trans 'G' = Just 'C'
  trans 'C' = Just 'G'
  trans 'T' = Just 'A'
  trans 'A' = Just 'U'
  trans _ = Nothing
