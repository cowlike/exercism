module Clock (clockHour, clockMin, fromHourMin, toString) where

import Text.Printf

newtype Clock = Clock Int deriving (Show, Eq)

instance Num Clock where
    (+) (Clock x) (Clock y) = Clock $ x + y
    (*) (Clock x) (Clock y) = Clock $ x * y
    abs c = c
    signum c = c
    fromInteger = Clock . fromIntegral
    negate (Clock n) = Clock $ (-n) `mod` 1440

clockHour :: Clock -> Int
clockHour (Clock n) = n `div` 60 `mod` 24

clockMin :: Clock -> Int
clockMin (Clock n) = n `mod` 60

fromHourMin :: Int -> Int -> Clock
fromHourMin hours minutes = Clock $ (hours * 60 +  minutes) `mod` 1440

toString :: Clock -> String
toString clock = printf "%0.2d:%0.2d" (clockHour clock) (clockMin clock)
