module Bob where

import Data.Char (toUpper, isSpace, isAlpha)

responseFor :: String -> String
responseFor x
  | null x || all isSpace x = "Fine. Be that way!"
  | any isAlpha x && map toUpper x == x = "Whoa, chill out!"
  | last x == '?' = "Sure."
  | otherwise = "Whatever."
