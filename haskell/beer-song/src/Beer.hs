module Beer (verse, sing) where

import Data.Char as C

verse :: Integer -> String
verse n
  | n > 0 = prefix ++ "Take " ++
            (if n == 1 then "it" else "one") ++
            " down and pass it around, " ++
            bottle (n - 1) ++ " of beer on the wall.\n"
  | otherwise = prefix ++ "Go to the store and buy some more, " ++
                "99 bottles of beer on the wall.\n"
  where prefix = capitalize (bottle n) ++ " of beer on the wall, " ++
                 bottle n ++ " of beer.\n"

-- Go to the store and buy some more, 99 bottles of beer on the wall.

capitalize :: String -> String
capitalize [] = []
capitalize (h:t) = C.toUpper h : map C.toLower t

bottle :: Integer -> String
bottle n
  | n == 0 = "no more bottles"
  | otherwise = show n ++ " bottle" ++ if n > 1 then "s" else ""

sing :: Integer -> Integer -> String
sing start end = concatMap ((++ "\n").verse) [start, start-1 .. end]
