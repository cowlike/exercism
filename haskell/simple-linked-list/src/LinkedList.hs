module LinkedList
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

-- The task is to create the data type `LinkedList`
-- and implement the functions below.

data LinkedList a =  Nil | Node a (LinkedList a) deriving Show

datum :: LinkedList a -> a
datum (Node a nxt) = a
datum _ = error "Empty list"

fromList :: [a] -> LinkedList a
fromList = foldr Node Nil

isNil :: LinkedList a -> Bool
isNil Nil = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new = Node

next :: LinkedList a -> LinkedList a
next (Node _ nxt) = nxt
_ = error "Empty list"

nil :: LinkedList a
nil = Nil

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList lst = rl lst Nil where
  rl Nil acc = acc
  rl (Node a nxt) acc = rl nxt (Node a acc)

toList :: LinkedList a -> [a]
toList Nil = []
toList (Node a nxt) = a : toList nxt
