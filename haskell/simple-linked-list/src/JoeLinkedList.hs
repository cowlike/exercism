module JoeLinkedList
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

import Data.List (reverse)

-- The task is to create the data type `LinkedList`
-- and implement the functions below. 

data LinkedList a = Node {value :: a, nextn :: LinkedList a} | Nil deriving Show

datum :: LinkedList a -> a
datum Node {value = v} = v
datum _ = error "Empty List"

fromList :: [a] -> LinkedList a
fromList = foldl (flip new) nil

isNil :: LinkedList a -> Bool
isNil Nil = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new v Nil = Node {value = v, nextn = Nil} 
new v l = Node {value = value l, nextn = new v $ nextn l}

next :: LinkedList a -> LinkedList a
next Node {nextn = n} = n
next _ = Nil

nil :: LinkedList a
nil = Nil

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList = fromList . reverse . toList

toList :: LinkedList a -> [a]
toList Nil = []
toList Node {value = v, nextn = n} = v : toList n
