module JoeLinkedListNorec
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

import Data.List (reverse)

-- The task is to create the data type `LinkedList`
-- and implement the functions below. 

data LinkedList a = Node a (LinkedList a) | Nil deriving Show

datum :: LinkedList a -> a
datum Nil = error "Empty List"
datum (Node v _) = v

fromList :: [a] -> LinkedList a
fromList = foldl (flip new) nil

isNil :: LinkedList a -> Bool
isNil Nil = True
isNil _ = False

new :: a -> LinkedList a -> LinkedList a
new v Nil = Node v Nil
new v (Node oldv l) = Node oldv (new v l) 

next :: LinkedList a -> LinkedList a
next Nil = Nil
next (Node _ l) = l

nil :: LinkedList a
nil = Nil

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList = fromList . reverse . toList

toList :: LinkedList a -> [a]
toList Nil = []
toList (Node v l) = v : toList l
