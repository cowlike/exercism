module Garden
    ( Plant (..)
    , defaultGarden
    , garden
    , lookupPlants
    ) where

import Data.Map (Map)
import qualified Data.Map as M
import Data.List(sort)
import Data.Maybe (fromMaybe)
import Data.List.Extra (chunksOf)

data Plant = Clover
           | Grass
           | Radishes
           | Violets
           | NOPLANT
           deriving (Eq, Show)

defaultGarden :: String -> Map String [Plant]
defaultGarden = garden students where
    students = ["Alice", "Bob", "Charlie", "David", 
                "Eve", "Fred", "Ginny", "Harriet", 
                "Ileana", "Joseph", "Kincaid", "Larry"]

garden :: [String] -> String -> Map String [Plant]
garden students plantList = M.fromList $ zipWith (\(a, xs) (_, ys) -> (a, xs ++ ys)) r1 r2
    where [r1,r2] = map (zip (sort students) . convertRow) $ lines plantList

lookupPlants :: String -> Map String [Plant] -> [Plant]
lookupPlants name g = fromMaybe [] $ M.lookup name g

convertRow :: String -> [[Plant]]
convertRow = chunksOf 2 . map toPlant

toPlant :: Char -> Plant
toPlant c = case c of
    'G' -> Grass
    'V' -> Violets 
    'C' -> Clover 
    'R' -> Radishes
    _ -> NOPLANT