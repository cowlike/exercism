module Sublist
  (
    Sublist(Equal, Sublist, Superlist, Unequal)
    , sublist
  ) where

import qualified Data.List as L

data Sublist = Equal | Sublist | Superlist | Unequal
  deriving (Eq, Show)

sublist :: Eq a => [a] -> [a] -> Sublist
sublist xs ys
  | xs == ys = Equal
  | length xs == length ys = Unequal
  | length xs > length ys = if elem ys $ partBy (length ys) xs then Superlist else Unequal
  | otherwise = if elem xs $ partBy (length xs) ys then Sublist else Unequal

partBy :: Int -> [a] -> [[a]]
partBy len xs = map (take len) $ L.tails xs
