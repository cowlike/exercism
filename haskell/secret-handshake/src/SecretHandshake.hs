{-# LANGUAGE FlexibleInstances #-}
module SecretHandshake (handshake) where

import Data.Maybe (fromMaybe)
import Data.List (foldl', elem)
import Data.Bits

class SecretHandshake a where
    handshake :: a -> [String]

instance SecretHandshake String where
    handshake = handshakeStr

instance SecretHandshake Int where
    handshake = handshakeInt

moves :: [String]
moves = ["wink", "double blink", "close your eyes", "jump"]

handshakeStr :: String -> [String]
handshakeStr s = fromMaybe [] $ handshakeInt <$> readBinary s

handshakeInt :: Int -> [String]
handshakeInt num = 
    f $ foldr (\(str, b) acc -> if b then str : acc else acc) [] (zip moves bitTests) where
    bitTests = map (testBit num) [0..3]
    f = if testBit num 4 then reverse else id

readBinary :: String -> Maybe Int
readBinary s = if all (`elem` "01") s then Just $ toInt s else Nothing where 
    toInt = foldl' (\acc c -> (acc * 2) +  if c == '0' then 0 else 1) 0
