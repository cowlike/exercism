module ETL (transform) where

import Data.Map (Map, empty, insert, foldrWithKey)
import Data.Char (toLower)

transform :: (Ord a) => Map a String -> Map Char a
transform = foldrWithKey (\k v acc -> foldr (insertNew k) acc v) empty where
    insertNew v k = insert (toLower k) v