module Robot
    ( Bearing(East,North,South,West)
    , bearing
    , coordinates
    , mkRobot
    , simulate
    , turnLeft
    , turnRight
    ) where

data Bearing = North
             | East
             | South
             | West
             deriving (Eq, Show, Enum)

type Coord = (Integer, Integer)

type Move = Char

newtype Robot = Robot (Bearing, Coord) deriving Show

bearing :: Robot -> Bearing
bearing (Robot (b, _)) = b

coordinates :: Robot -> (Integer, Integer)
coordinates (Robot (_, coord)) = coord

mkRobot :: Bearing -> (Integer, Integer) -> Robot
mkRobot b coord = Robot (b, coord)

simulate :: Robot -> String -> Robot
simulate = foldl moveBot

moveBot :: Robot -> Move -> Robot
moveBot (r@(Robot (b, c))) m
    | m == 'A' = advance r
    | m == 'L' = Robot (turnLeft b, c)
    | m == 'R' = Robot (turnRight b, c)
    | otherwise = r

turnLeft :: Bearing -> Bearing
turnLeft b
    | b == North = West
    | otherwise = pred b

turnRight :: Bearing -> Bearing
turnRight b
    | b == West = North
    | otherwise = succ b

advance :: Robot -> Robot
advance (Robot (North, (x, y))) = Robot (North, (x, succ y))
advance (Robot (South, (x, y))) = Robot (South, (x, pred y))
advance (Robot (East, (x, y))) = Robot (East, (succ x, y))
advance (Robot (West, (x, y))) = Robot (West, (pred x, y))