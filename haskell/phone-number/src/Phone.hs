module Phone (areaCode, number, prettyPrint) where

import Data.Char(isDigit)
import Text.Printf

areaCode :: String -> Maybe String
areaCode n = take 3 <$> number n

number :: String -> Maybe String
number str
  | len == 10 = Just pn
  | len == 11 && head pn == '1' = Just $ drop 1 pn
  | otherwise = Nothing
  where pn = take 12 $ filter isDigit str
        len = length pn

prettyPrint :: String -> Maybe String
prettyPrint s = printf "(%s) %s-%s" 
  <$> areaCode s 
  <*> (take 3 . drop 3 <$> number s) 
  <*> (drop 6 <$> number s)
