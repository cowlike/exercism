module Roman (numerals) where

convert :: Int -> Int -> [String]
convert decimalPlace n
    | decimalPlace < 0 || decimalPlace > 3 = []
    | n == 4 = [d0, d1]
    | n == 5 = [d1]
    | n == 9 = [d0, d2]
    | n < 4 = replicate n d0
    | n < 9 = d1 : replicate (n - 5) d0
    | otherwise = []
    where 
        digits = [("I","V"), ("X","L"), ("C","D"), ("M","?"), ("?","?")]
        (d0,d1) = digits !! decimalPlace
        (d2,_) = digits !! (decimalPlace + 1)

numerals :: Int -> String
numerals num = 
    (concat.concat.reverse) (zipWith convert [0..] (decimal [] num)) where
    decimal lst n
        | n == 0 = lst
        | otherwise = decimal (lst ++ [n `mod` 10]) (n `div` 10)
    
