module RunLength (decode, encode) where

import Data.List (group)
import Data.Char (isDigit)

digits :: String -> (Int, String)
digits s = let (l,xs) = span isDigit s in (read l,xs)

decode :: String -> String
decode [] = []
decode s | isDigit $ head s = let (l, y:ys) = digits s in replicate l y ++ decode ys
decode (x:xs) = x : decode xs
    
encode :: String -> String
encode = concatMap mkstr . group where
    mkstr [] = []
    mkstr [x] = [x]
    mkstr s@(x:_) = show (length s) ++ [x]
