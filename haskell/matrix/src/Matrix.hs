module Matrix
    ( Matrix
    , cols
    , column
    , flatten
    , fromList
    , fromString
    , reshape
    , row
    , rows
    , shape
    , transpose
    ) where

import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Data.List as L

newtype Matrix a = Matrix (Vector (Vector a)) deriving Show

instance (Read a) => Read (Matrix a) where
    readsPrec _ s = [(readMatrix s, "")]

instance Eq a => Eq (Matrix a) where
    (==) (Matrix v1) (Matrix v2) = v1 == v2

cols :: Matrix a -> Int
cols = rows . transpose

column :: Int -> Matrix a -> Vector a
column n = row n . transpose

flatten :: Matrix a -> Vector a
flatten (Matrix v) = L.foldl' (V.++) V.empty v

fromList :: [[a]] -> Matrix a
fromList list = Matrix $ V.fromList $ map V.fromList list

fromString :: Read a => String -> Matrix a
fromString = read

chunk :: Int -> Vector a -> Vector (Vector a)
chunk n v = V.reverse $ chunksOf n v V.empty where
    chunksOf size vec acc
        | V.null vec = acc
        | otherwise = let (f,s) = V.splitAt size vec in
            chunksOf size s $ V.cons f acc

reshape :: (Int, Int) -> Matrix a -> Matrix a
reshape (_,c) m = Matrix $ chunk c $ flatten m

row :: Int -> Matrix a -> Vector a
row n (Matrix r) = r V.! n

rows :: Matrix a -> Int
rows (Matrix r) = V.length r

shape :: Matrix a -> (Int, Int)
shape m = (rows m, cols m)

transpose :: Matrix a -> Matrix a
transpose = fromList . L.transpose . toList

toList :: Matrix a -> [[a]]
toList (Matrix m) = V.toList $ V.map V.toList m

splitChar :: Char -> String -> [String]
splitChar _ [] = []
splitChar c (x:xs) | x == c = splitChar c xs
splitChar c s = takeWhile (/= c) s : splitChar c (dropWhile (/= c) s)

readMatrix :: Read a => String -> Matrix a
readMatrix s = Matrix $ V.fromList $ map ((V.fromList . map read) . foo s) $ lines s where
    foo ('"':_) = map stringify . filter (not . all (== ' ')) . splitChar  '"'
    foo _ = words
    stringify s' = "\"" ++ s' ++ "\""
