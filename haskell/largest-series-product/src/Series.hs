module Series (largestProduct) where

import Data.Char (isDigit, digitToInt)
import Data.List.Split (divvy)
import Control.Monad (guard)

largestProduct :: Int -> String -> Maybe Int
largestProduct n digits = do
    guard $ n >= 0 && n <= length digits
    guard $ all isDigit digits
    return $ if n == 0 || null digits 
             then 1 
             else maximum $ map (product . map digitToInt) $ divvy n 1 digits
