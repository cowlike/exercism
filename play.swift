func iterate<T> (times: Int, _ initial: T, _ f: ((_:T) -> T)) -> T {
	if (times == 0) { return initial }
	
	return f(iterate(times: (times - 1), initial, f))
}

func nTimes<T> (times: Int, _ initial: T, _ f: ((_:T) -> T)) -> [T] {
	return (1 ..< times).reduce([f(initial)]) { 
		(acc,_) in return acc + [f(acc.last!)] 
	}
}

func getRow(_ currentRow: [Int]) -> [Int] {
    let sum = { (xs:[Int]) in xs.reduce(0, +) }
	return currentRow.isEmpty ? [1] : 
		(currentRow.windowed(2).reduce([1], { $0 + [sum($1)] }) + [1])
}

extension Array {
    func windowed(_ size: Int, _ index: Int = 0) -> [[Element]] {
        if (size + index > self.count) { 
            return [] 
        }
        else {
            let x = self.startIndex + index
            return [Array(self [x ..< x + size])] + windowed(size, index + 1)
        }   
    }
}

print(nTimes(times: 3, 1, {$0 * 2}))
print(nTimes(times: 4, [], getRow))


//=======================================

class Fibonacci : Sequence {
  func makeIterator() -> FibonacciGenerator {
    return FibonacciGenerator()
  }
}

class FibonacciGenerator : IteratorProtocol {
  var current = 0, nextValue = 1

  //typealias Element = Int

  func next() -> Int? {
    let ret = current
    current = nextValue
    nextValue = nextValue + ret
    return ret
  }
}

let fib = Fibonacci()

for x in fib.prefix(10) { print(x) }
for x in fib.dropFirst(20).prefix(10) { print(x) }
