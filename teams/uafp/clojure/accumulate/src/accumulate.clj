(ns accumulate)

;; not tail recursive but the test cases are tiny

(defn accumulate [f xs]
  (if (empty? xs)
    xs
    (cons (f (first xs)) (accumulate f (rest xs)))))
