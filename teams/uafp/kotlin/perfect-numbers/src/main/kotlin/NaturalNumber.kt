enum class Classification {
  DEFICIENT, PERFECT, ABUNDANT
}

fun classify(n: Int) = require(0 < n).run {
  (1..n / 2)
    .fold(0) { acc, x -> if (n % x == 0) acc + x else acc }
    .let {
      when {
        it == n -> Classification.PERFECT
        it > n -> Classification.ABUNDANT
        else -> Classification.DEFICIENT
      }
    }
}
/*
fun classify(naturalNumber: Int): Classification {
//  require(naturalNumber > 0)
  require(naturalNumber > 0) { "Argument must be > 0" }

  val sum = (1 until naturalNumber)
    .fold(0) { acc, x -> if (naturalNumber.rem(x) == 0) acc + x else acc }
  return when {
    sum == naturalNumber -> Classification.PERFECT
    sum > naturalNumber -> Classification.ABUNDANT
    else -> Classification.DEFICIENT
  }
}
*/
//fun classify(n: Int): Classification {
//  require(n > 0) { "Argument must be > 0" }
//
//  val aliquotSum = (1..n / 2)
//    .sumBy { if (n % it == 0) it else 0 }
//
//  return when {
//    aliquotSum < n -> Classification.DEFICIENT
//    aliquotSum == n -> Classification.PERFECT
//    else -> Classification.ABUNDANT
//  }
//}
