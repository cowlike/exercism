import kotlin.properties.Delegates

class Reactor<T>() {
  interface Subscription {
    fun cancel()
    fun callback()
  }

  abstract class Cell<T>() {
    abstract var value: T
    var observers: List<Cell<T>> = emptyList()
    open fun notify1() {}
  }

  inner class InputCell<T>(var v: T) : Cell<T>() {
    override var value by Delegates.observable(v) { _, old, new ->
      if (old != new) {
        observers.forEach { it.notify1() }
      }
    }
  }

  inner class ComputeCell<T>(vararg cells: Cell<T>, val computation: (List<T>) -> T) : Cell<T>() {
    private var sources = cells
    private var prev: T = value

    init {
      sources.forEach { it.observers += this }
    }

    override fun notify1() {
      value = computation(sources.map { it.value })
    }

    override var value: T
      get() = computation(sources.map { it.value })
      set(v) {
        if (prev != v) {
          prev = v
          subs.values.forEach { it.callback() }
          observers.forEach { it.notify1() }
        }
      }

    val subs = mutableMapOf<Int, Subscription>()

    fun addCallback(cb: (T) -> Unit): Subscription {
      val subscription = object : Subscription {
        override fun cancel() {
          subs.remove(this.hashCode())
        }

        override fun callback() = cb(value)
      }
      subs[subscription.hashCode()] = subscription
      return subscription
    }
  }
}
