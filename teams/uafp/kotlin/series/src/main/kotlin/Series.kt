object Series {
  fun slices(size: Int, input: String) =
    require(input.isNotEmpty() && size <= input.length).run {
      input.map { it - '0' }.windowed(size, 1)
    }
}
