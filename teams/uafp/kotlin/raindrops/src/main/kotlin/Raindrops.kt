class Raindrops {

  companion object {
    fun convert(num: Int) =
      (3 .. num).fold("") { acc, n ->
        num.rem(n).let {
          if (it != 0) acc
          else when (n) {
            3 -> acc + "Pling"
            5 -> acc + "Plang"
            7 -> acc + "Plong"
            else -> acc
          }
        }
      }.let { drops -> if (drops.isNotEmpty()) drops else num.toString() }
  }
}
//    companion object {
//        fun convert(num: Int): String {
//            return (1..num).filter {
//                num.rem(it) == 0 && (it == 3 || it == 5 || it == 7)
//            }.fold("") { acc, n ->
//                when (n) {
//                    3 -> acc + "Pling"
//                    5 -> acc + "Plang"
//                    7 -> acc+"Plong"
//                    else -> acc
//                }
//            }.let { raindrops -> if(raindrops.isNotEmpty()) raindrops else num.toString() }
//
//        }
//    }
//}