class Series(private val digits: String) {
  init {
    require(digits.all { it.isDigit() })
  }

  fun getLargestProduct(n: Int): Int {
    require(n <= digits.length)
    return if (n == 0 || digits.isEmpty()) 1 else maxProduct(n)
  }

  private val maxProduct = ::maxProduct1

  private fun maxProduct1(n: Int) =
    digits.windowed(n, 1) {
      it.fold(1) { acc, v -> acc * (v - '0') }
    }.max() ?: 0

  //------------------------
  // using product() helper
  private fun List<Int>.product() = this.fold(1, Int::times)

  private fun maxProduct2(n: Int) =
    digits.windowed(n, 1) { it.map { x -> x - '0' }.product() }.max() ?: 0
}
