class RotationalCipher(val key: Int) {

  private fun Char.rotateFrom(start: Char) =
    ((this - start + key) % 26 + start.toInt()).toChar()

  fun encode(input: String) = input.map {
    when {
      it.isLowerCase() -> it.rotateFrom('a')
      it.isUpperCase() -> it.rotateFrom('A')
      else -> it
    }
  }.joinToString("")
}
