class Robot(
    var gridPosition: GridPosition = GridPosition(0, 0),
    var orientation: Orientation = Orientation.NORTH) {

  fun turnRight(n: Int = 1) {
    orientation = Orientation.values().get((orientation.ordinal + n) % 4)
  }

  fun turnLeft() {
    turnRight(3)
  }

  private operator fun GridPosition.plus(pair: Pair<Int, Int>) =
      GridPosition(x = this.x + pair.first, y = this.y + pair.second)

  fun advance() {
    gridPosition = when (orientation) {
      Orientation.NORTH -> gridPosition + (0 to 1)
      Orientation.SOUTH -> gridPosition + (0 to -1)
      Orientation.EAST -> gridPosition + (1 to 0)
      Orientation.WEST -> gridPosition + (-1 to 0)
    }
  }

  fun simulate(instructions: String) {
    instructions.forEach { move ->
      when (move) {
        'R' -> turnRight()
        'L' -> turnLeft()
        'A' -> advance()
        else -> Unit
      }
    }
  }
}