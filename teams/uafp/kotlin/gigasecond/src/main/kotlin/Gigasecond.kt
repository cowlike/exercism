import java.time.LocalDate
import java.time.LocalDateTime

class Gigasecond (localDate: LocalDate)
{
  var date = {
    println("setting date\n")
    writeSomething()
    localDate.atStartOfDay().plusSeconds(1000000000)
  }()


  constructor(localDateTime: LocalDateTime): this(localDateTime.toLocalDate()) {
    println("secondary constructor\n")
    date = localDateTime.plusSeconds(1000000000)
  }

  fun writeSomething() {
    println("hi!\n")
  }
}
