//package Pangram
/*
A top level function in a package
 */
fun isPangram(p: String) = (('a'..'z') - p.toLowerCase().toSet()).isEmpty()

class Pangram {
    companion object {
        /*
        see: https://kotlinexpertise.com/coping-with-kotlins-scope-functions/
         */
        private fun toSet(s: String) = s.run { toLowerCase().toSet() }

        private fun isPangram0(p: String) = (('a'..'z') - toSet(p)).isEmpty()

        private fun isPangram1(p:String) = ('a'..'z').all {
            toSet(p).contains(it)
        }

        private fun isPangram2(p:String) = toSet(p).let {
            s -> ('a'..'z').all { it in s }
        }

        val isPangram = ::isPangram2
    }
}


