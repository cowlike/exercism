private val letters = ('a'..'z').map { it }

private fun randomString() = (1..100).let { nums ->
  val rand = java.util.Random()
  nums.map { letters[0] + rand.nextInt(letters.size) }.joinToString("")
}

class Cipher(val key: String = randomString()) {

  private val shift: Array<Int>

  init {
    require(key.isNotEmpty() && key.all { it in letters })
    shift = key.fold(emptyArray()) { acc, v ->
      acc + (v - letters[0])
    }
  }

  private fun translate(text: String, f: (Int) -> Int): String {
    require(text.all { it in letters })

    return text.foldIndexed(StringBuffer()) { idx, acc, c ->
      acc.append(letters[(c - letters[0] + f(shift[idx])) % letters.size])
    }.toString()
  }

  fun encode(plainText: String) = translate(plainText) { it }

  fun decode(cipherText: String) = translate(cipherText) { letters.size - it }
}
