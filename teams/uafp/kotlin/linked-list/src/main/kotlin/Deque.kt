class Deque<T> {
  data class Node<T>(
    var value: T,
    var prev: Node<T>? = null,
    var next: Node<T>? = null
  )

  private var first: Node<T>? = null
  private var last: Node<T>? = null

  fun push(v: T) {
    val f = first
    first = Node(v, next = f)
    f?.prev = first
    if (last == null) last = first
  }

  fun pop(): T? {
    val result = first?.value
    first = first?.next
    return result
  }

  fun unshift(v: T) {
    val l = last
    last = Node(v, prev = l)
    l?.next = last
    if (first == null) first = last
  }

  fun shift(): T? {
    val result = last?.value
    last = last?.prev
    return result
  }
}
