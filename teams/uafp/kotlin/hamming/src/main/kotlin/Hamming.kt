class Hamming {
  companion object {
    fun compute1(xs: String, ys: String) =
      require(xs.length == ys.length) { "left and right strands must be of equal length." }
        .run { xs.zip(ys) { a, b -> if (a != b) 1 else 0 }.sum() }

    //Another way using explicit recursion

    private fun oops(): Nothing =
      throw IllegalArgumentException("left and right strands must be of equal length.")

    fun compute2(xs: String, ys: String): Int {
      return when {
        xs.isEmpty() && ys.isEmpty() -> 0
        xs.isEmpty() && ys.isNotEmpty() -> oops()
        xs.isNotEmpty() && ys.isEmpty() -> oops()
        else -> (if (xs.first() != ys.first()) 1 else 0) +
            compute2(xs.drop(1), ys.drop(1))
      }
    }

    fun compute3(str1: String, str2: String): Int {
      require(str1.length == str2.length) { "left and right strands must be of equal length." }

      return str1.foldIndexed(0) { x, acc, v -> acc + if (str1[x] == str2[x]) 0 else 1 }
    }

    val compute = ::compute2
  }
}
