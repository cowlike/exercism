type 'a Tree = Nil | Tree of 'a Tree * 'a * 'a Tree

type MyBool = Yes | No | Maybe

type Person = Person of MyBool * bool

type PersonRec = { name : string; age : int }

let foo mb = 
    match mb with
    | Yes -> "yes"
    | No -> "no"
    | _ -> "who knows?"

let p = Person (Yes, true)

let bar person = 
    match person with
    | Person(mb, false) when mb = Maybe -> 1
    | Person(Maybe, _) -> 2
    | _ -> 3

let p2 = { name = "john"; age = 20 }

printfn "%A" p2

let r = 15 + if p2.name = "jack" then 1 else 2



let rec toList = function
    | Nil -> []
    | Tree (l, d, r) -> toList l @ [d] @ toList r

let isBinary tree = let lst = toList tree in lst = List.sort lst

//true
//isBinary <| Tree (Tree (Nil, 0, Nil), 3, Tree (Nil, 5, Tree (Tree (Nil, 7, Nil), 8, Nil)))

//false
//isBinary <| Tree (Tree (Nil, 0, Nil), 3, Tree (Nil, 7, Tree (Tree (Nil, 5, Nil), 8, Nil)))

let x = 4 + 3